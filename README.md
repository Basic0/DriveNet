# DriveNet

## So What Is It?

DriveNet was my attempt to understand neural networks by evolving one capable of driving a car around a track.

Each driver is a unique neural network. For the first round, those neural networks are generated entirely at random.

Vehicles are placed on a test track, each with a different driver assigned. Vehicles pass sensor data to drivers who convert that to into steering and throttle commands.

The progress of each driver is evaluated and scored.

The highest scoring drivers are used to seed a new generation, then the process repeats.

A dozen generations is usually sufficient to make a complete circuit, and subsequent generations develop better lap times.

[![Evolution Video](Assets/Docs/Thumbs/EarlyGenerations.png)](https://www.youtube.com/watch?v=SL2PXJnB08g)

By generation 100, drivers are following a racing line and turning into slides.

[![Generation 100 Video](Assets/Docs/Thumbs/Generation100.png)](https://www.youtube.com/watch?v=20jaUUlImAM)

## Quick Start

Download: [DriveNet-Winx64AnyCPU.zip](PreBuilt/DriveNet-Winx64AnyCPU.zip)

Unzip and run DriveNet.exe

Press `F1` after starting a run to see keyboard shortcuts

#### Bootstrap Your Evolution
Download [L-L.Gen0.json.deflate](Assets/Docs/L-L.Gen0.json.deflate) and place it in the `DriveNet` folder in `My Documents` (created at first launch)

Then start a new run using Network Configuration 12-12-None. Ensure [Load Matching Historical Data](#load-matching-historical-data) is ticked.

## Caveat - Network Connection

This is built on top of the Unity game engine which attempts to phone home on first launch with a new random ID and some hardware stats (CPU/RAM/GPU). In the free version, I'm unable to completely disable these analytics.

The application does not require internet access, so feel free to block it in your firewall and/or build it yourself so you have confidence.

## The Details

### Test Configuration

On launch you are presented with the test configuration screen.

![Input area of the UI](Assets/Docs/TestOptions.png)

##### Genotypes Per Generation

Each generation, this number of candidates will be generated and tested. Whatever number you select will be doubled on the first round (where genotypes are purely random) to try to ensure sufficient diversity

##### Fittest Genotypes To Breed

After each round, all candidates are ranked by score, and a breeding pool is selected

This value indicates the number of top candidates to place in the pool

##### Mutation Rate

When generating candidate, this controls how much chance there is for **each euron** to mutate (as opposed to inheriting from a parent)

See [Generating Candidates](#generating-candidates) below for more information on the mutation process

##### Include Best Ancestor Genotype

In addition to the fittest genotypes from the previous round, additionally take the highest scoring genotype of all time and include that in the breeding pool.

This has the advantage of ensuring that valuable traits/genes are harder to lose, however it also acts as an anchor, potentially keeping a promising but dead-end solution around longer than it might otherwise last.

##### Include Worst Genotype Pair

As well as all the fit candidates above, also take two particularly low-scoring candidates from the last round and place them in the breeding pool too.

The goal of this options is it to prevent over-fitting, especially in dead-end situations. A similar effect could be achieved (possibly better) with a higher mutation rate, but that increases time to converge on a solution.

##### Max Active Test Vehicles

How many vehicles to keep on the track at any one time. This is largely driven by hardware and how many tests your machine supports running in parallel.

During a test run, press `F` to show FPS. The physics engine starts introducing minor errors under 60FPS and can become unstable below 15 FPS.

Note also that this is a goal. The spawn rate is limited, so if a large number of vehicles are failing quickly (as in early rounds) this limit may not be reached.

##### Vehicle Lights

Does what it says on the tin

##### Hidden Layer Neurons

These settings allow you to define the topology of your neural network.

More nodes means a larger, more complex model.

Larger networks will take longer to train, but they can potentially handle more complex driving models



###### How many is enough?

Good question. That's what I wrote this to try and explore.

There's some theory that can be applied if the problem domain is very well understood, however, in this case we're using a physics engine and virtual world to define the problem. 

##### Load Matching Historical Data

After every round completes, run data is written to disk. 

When starting a new test, this tells the test runner to find any previous runs with the same neural network topology and load them, allowing tests to be continued/extended.

##### Track Direction

Choose which direction to use when testing

This mitigates against [Overfitting](#overfitting)

##### Move Finish Line Every Round

Every round move the finish line approximately 1% of the way around the track.

This mitigates against [Overfitting](#overfitting)

### Overfitting

Given enough time, candidates will eventually train to the point where they're aiming for rounding errors in the physics engine, cutting corners so closely as to be unreliable.

To help fight this, the [Move Finish Line Every Round](#move-finish-line-every-round) and [Track Direction](#track-direction) options allow for starting conditions to vary, forcing a general solution.

### Scoring

Each driver is evaluated and given a score for every run.

If a driver meets any of the following conditions, their run will be aborted and no more points scored:

* Leave the track ["Off Track"]
* Head wrong way around track for more than 3 segments ["Wrong Direction"] (10 segments if at the start line)
* Make no progress along the track for 5 seconds ["Stalled"]
* Take longer than 90 seconds ["Timed Out"]

Every driver receives 1 point for each track segment they complete -sequentially- in the correct direction. There are 1,009 track segments.

If the driver crosses the finish line, then in addition to the above, they receive a bonuses:

* 100 points for every second under 90 seconds to complete the lap.
* Top speed (in m/s) converted into points.


### Driving Inputs

The system generates three goal markers for each vehicle (near/mid/far) that are placed a fixed distance ahead of the vehicle in the centre of the track, as shown here:

[![Video showing goal markers](Assets/Docs/Thumbs/GoalMarkers.png)](https://www.youtube.com/watch?v=ZIhQW9Py6u8)

The sensor suite fetches the following information every frame:

* Current speed in m/s
* Near goal marker bearing
* Near goal marker relative height
* Mid goal marker bearing
* Mid goal marker relative height
* Far goal marker bearing
* Far goal marker relative height

Note that for the goal markers, horizontal/x is an angle, whereas vertical/y is an absolute offset. This was chosen as the vertical variation is considerably more subtle than horizontal.


### Driving

Each frame, the vehicle takes the data from the sensor suite above and normalises it into the range -1 to 1 (required for the network to process the data).

![Input area of the UI](Assets/Docs/InputView.png)

For each of the inputs here are the ranges supported. Anything above/below these values will clamp:

##### Current Speed
Linear mapping where -1 represents -5m/s and +1 represents 55m/s

##### Goal Markers (Horizontal)
For each goal marker the relative angle is mapped so -1 represents -90° (left), and 1 represents 90° (right).

##### Goal Markers (Vertical)
For each goal marker the relative height between the vehicle and the goal is mapped so -1 represents a goal 10m below the vehicle and 1 represents 10m above.


## The Neural Network

Each driver's neural network consists of one input layer, between 1 and 3 hidden layers (chosen by the user), and an output layer

![Input area of the UI](Assets/Docs/NeuralNetView.png)

In the visualisation, each neuron is represented by a coloured dot. The colour indicates the current value of the neuron.

![Hues](Assets/Docs/Hue.png)

The top row indicates the 7 inputs and the bottom row shows the desired steering and throttle.

The white indicators to the left and bottom of the network show the commanded throttle and steering respectively.

The strength of a particular neuron-neuron connection is represented by how opaque it is. Weaker connections are more transparent.

Finally, neuron-neuron connections are coloured by the signal they're currently relaying.

Network topography is chosen at the start of the test and all generations must have the same topology.

Internally, the neurons of the network are defined as a series of tanh operations with a `weight` and `bias`


### Generating Candidates

After each round, a breeding pool is generated according to the test configuration.

Pairs of candidates are selected to have their networks merged (and mutated) to generate a new candidate.

The process to generate a candidate is as follows:

Select 2 candidates

For each neuron from each hidden layer:

	If(RandomNumber > MutationRate) {
		Pick a random parent [50/50 chance] and use their weight and bias.
	} else {
		One of 4 scenarios, all equally likely:
			* Use weight from a random parent, and bias from the other
			* Use weight from a random parent, and a random bias
			* Use a random weight, and a bias from a random parent
			* Use a random weight and random bias
	}

**Note:** This is happening **per-neuron** meaning the actual mutation rate is a function of both the specified mutation rate and the number of neurons.


## Running DriveNet

### From Pre-built Binaries

##### Requires
* .Net Framework 4.6

Download the release archive and extract locally.

[DriveNet-Winx64AnyCPU.zip](PreBuilt/DriveNet-Winx64AnyCPU.zip)

Launch `DriveNet.exe`

### Building from source

##### Requires

* Unity 2019.3.0f6
* .Net Framework 4.6 [installed automatically with Unity]

Clone the respository and open the project in Unity

Open the main `File` menu and select `Build Settings...`

Choose `Build` or `Build and Run` as appropriate, you will be prompted for an output location.

All dependencies and resources are included in the main repository.

### Run data

After each run, a selection of best/worst genomes along with statistics, lap times, etc is stored to disk.

These records are kept in a series of files under, one for each generation of a given neural network configuration.

> %userprofile%\Documents\DriveNet

(A new DriveNet folder created in "My Documents")

Records are retrieved by name, so renaming/moving records will prevent them being loaded in future.

### Logs

Process log data is kept and can be found in 

> %userprofile%\appdata\locallow\SOWare\DriveNet\player.log

If anything breaks, the stack trace should be here.

### Removal

To uninstall DriveNet, delete the following locations:

* The folder containing the DriveNet binary
* `%userprofile%\Documents\DriveNet`
* `%userprofile%\appdata\locallow\SOWare`

## Known issues

* Lap split times are calculated assuming all training runs use the same test configuration. Switching from eg Reverse to Forwards track direction will show splits from a run in the other direction, which are essentially meaningless.
* When the start line moves every round, splits are meaningless so are hidden (but still calculated internally as "jump to best split" _may_ potentially be useful [Down Arrow]
* When the Neural network window is unhidden, it doesn't start re-drawing until the mode is cycled [Press "n"] or a new vehicle selected [Arrow keys]
* Camera free-fly would be useful
