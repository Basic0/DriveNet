﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpUI : MonoBehaviour {

	GameObject panel;
	// Start is called before the first frame update
	void Start() {
		panel = transform.Find("Panel").gameObject;

	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKeyDown(KeyCode.F1)) {
			panel.SetActive(!panel.activeInHierarchy);
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
			panel.SetActive(false);
		}
	}
}