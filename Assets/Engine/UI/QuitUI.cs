﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using UnityEngine;

public class QuitUI : MonoBehaviour {

	[DllImport("user32.dll")]
	private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);
	[DllImport("user32.dll")]
	private static extern IntPtr GetActiveWindow();

	GameObject panel;
	// Start is called before the first frame update
	private bool visible = false;
	void Start() {
		panel = transform.Find("Panel").gameObject;

	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (visible) {
				CancelQuit();
			} else {
				Show();
			}
		}
		if (visible
			&& (
				   Input.GetKeyDown(KeyCode.Return)
				|| Input.GetKeyDown(KeyCode.KeypadEnter)
				|| Input.GetKeyDown(KeyCode.Space)
			)) {
			DoQuit();
		}
		if (visible
			&& Input.GetKeyDown(KeyCode.F1)) {
			CancelQuit();
		}
	}

	public void Show() {
		panel.SetActive(true);
		visible = true;
	}

	public void RunMiminised() {
		ShowWindow(GetActiveWindow(), 2);
		CancelQuit();

	}

	public void CancelQuit() {
		panel.SetActive(false);
		visible = false;
	}

	public void DoQuit() {
		Application.Quit(0);
	}
}
