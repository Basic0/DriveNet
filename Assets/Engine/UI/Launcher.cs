﻿using DriveNet.Assets.Engine.Core;
using DriveNet.Assets.Engine.Core.NeuralNetwork;
using Michsky.UI.ModernUIPack;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Launcher : MonoBehaviour {
	// Start is called before the first frame update

	HorizontalSelector genePoolSizeCtl;
	HorizontalSelector survivalSizeCtl;
	HorizontalSelector layerCtl1;
	HorizontalSelector layerCtl2;
	HorizontalSelector layerCtl3;
	HorizontalSelector vehicleCountCtl;
	HorizontalSelector mutationRateCtl;
	HorizontalSelector directionCtl;
	Toggle includeBestCtl;
	Toggle includeWorstCtl;
	Toggle loadHistoryCtl;
	Toggle carLightsCtl;
	Toggle advanceGoalEveryRoundCtl;
	void Start() {
		FindControls();
	}


	void FindControls() {
		genePoolSizeCtl = GameObject.Find("GenotypesPerGeneration").GetComponent<HorizontalSelector>();
		survivalSizeCtl = GameObject.Find("BreedCount").GetComponent<HorizontalSelector>();
		layerCtl1 = GameObject.Find("HiddenLayer1").GetComponent<HorizontalSelector>();
		layerCtl2 = GameObject.Find("HiddenLayer2").GetComponent<HorizontalSelector>();
		layerCtl3 = GameObject.Find("HiddenLayer3").GetComponent<HorizontalSelector>();
		vehicleCountCtl = GameObject.Find("VehicleCount").GetComponent<HorizontalSelector>();
		mutationRateCtl = GameObject.Find("MutationRate").GetComponent<HorizontalSelector>();
		directionCtl = GameObject.Find("Direction").GetComponent<HorizontalSelector>();


		includeBestCtl = GameObject.Find("IncludeBest").GetComponent<Toggle>();
		includeWorstCtl = GameObject.Find("Diversify").GetComponent<Toggle>();
		loadHistoryCtl = GameObject.Find("LoadHistorical").GetComponent<Toggle>();
		carLightsCtl = GameObject.Find("VehicleLights").GetComponent<Toggle>();
		advanceGoalEveryRoundCtl = GameObject.Find("AdvanceGoal").GetComponent<Toggle>();

	}

	public void BeginTesting() {

		int genePoolSize = int.Parse(genePoolSizeCtl.elements[genePoolSizeCtl.index], System.Globalization.NumberStyles.Integer | System.Globalization.NumberStyles.AllowThousands);
		int survivalSize = int.Parse(survivalSizeCtl.elements[survivalSizeCtl.index], System.Globalization.NumberStyles.Integer | System.Globalization.NumberStyles.AllowThousands);

		int layer1 = 0;
		int layer2 = 0;
		int layer3 = 0;
		int.TryParse(layerCtl1.elements[layerCtl1.index], System.Globalization.NumberStyles.Integer | System.Globalization.NumberStyles.AllowThousands, null, out layer1);
		int.TryParse(layerCtl2.elements[layerCtl2.index], System.Globalization.NumberStyles.Integer | System.Globalization.NumberStyles.AllowThousands, null, out layer2);
		int.TryParse(layerCtl3.elements[layerCtl3.index], System.Globalization.NumberStyles.Integer | System.Globalization.NumberStyles.AllowThousands, null, out layer3);


		int vehicleCount;
		int.TryParse(vehicleCountCtl.elements[vehicleCountCtl.index], System.Globalization.NumberStyles.Integer | System.Globalization.NumberStyles.AllowThousands, null, out vehicleCount);

		float mutationRate = 0;
		if (mutationRateCtl.elements[mutationRateCtl.index] == "Chernobyl") {
			mutationRate = 5;
		} else {
			float.TryParse(mutationRateCtl.elements[mutationRateCtl.index].Replace("%", ""), System.Globalization.NumberStyles.Float, null, out mutationRate);
		}
		mutationRate /= 100;

		bool includeBest = includeBestCtl.isOn;
		bool includeWorst = includeWorstCtl.isOn;
		bool loadHistory = loadHistoryCtl.isOn;


		bool carLights = carLightsCtl.isOn;


		switch (directionCtl.elements[directionCtl.index]) {
			case "Forward":
				FinishLine.ReverseDirection = false;
				FinishLine.FlipFlopDirection = false;
				break;
			case "Reverse":
				FinishLine.ReverseDirection = true;
				FinishLine.FlipFlopDirection = false;
				break;
			case "Flip Flop":
				FinishLine.ReverseDirection = false;
				FinishLine.FlipFlopDirection = true;
				break;
			default:
				throw new ArgumentException(String.Format("Track Direction '{0}' not supported", directionCtl.elements[directionCtl.index]));
		}

		bool advanceGoalEveryRound = advanceGoalEveryRoundCtl.isOn;

		var layers = new List<int>();
		layers.Add(layer1);
		if (layer2 > 0) {
			layers.Add(layer2);
		}
		if (layer3 > 0) {
			layers.Add(layer3);
		}



		var config = new RunConfig {
			Id = Guid.NewGuid(),
			GenePoolSize = genePoolSize,
			SurvivalSize = survivalSize,
			MutationRate = mutationRate,
			KeepFittest = survivalSize,
			HiddenLayers = layers,
			IncludeHistoricalBest = includeBest,
			IncludeRoundWorst = includeWorst,
			LoadHistory = loadHistory,
			ConcurrentVehicles = vehicleCount,
			StartForward = !FinishLine.ReverseDirection,
			AlternateDirection = FinishLine.FlipFlopDirection,
			AdvanceGoal = advanceGoalEveryRound,
		};

		SetWorldToConfig(config);

		TestRunner.MaxConcurrentTests = vehicleCount;
		CarPool.LightsOn = carLights;
		//Fragile, you say?
		var cg = GameObject.Find("LaunchUI").GetComponent<CanvasGroup>();
		cg.alpha = 0;
		cg.interactable = false;
		cg.blocksRaycasts = false;
		Resources.FindObjectsOfTypeAll<CanvasGroup>().Where(x => x.name == "InGameUI").First().gameObject.SetActive(true);
		GameObject.Find("TestRunner").GetComponent<TestPlanner>().enabled = true;

	}

	private void SetWorldToConfig(RunConfig config) {
		Debug.Log("Setting environment to:\n" + JsonConvert.SerializeObject(config, Formatting.Indented));
		
		FinishLine.AdvanceGoal = config.AdvanceGoal;
		FinishLine.ReverseDirection = !config.StartForward;
		FinishLine.FlipFlopDirection = config.AlternateDirection;

		TestPlanner.roundSize = config.GenePoolSize;
		TestPlanner.winnerCount = config.SurvivalSize;
		TestPlanner.includeTheBest = config.IncludeHistoricalBest;
		TestPlanner.includeTheWeakest = config.IncludeRoundWorst;
		TestPlanner.loadHistory = true;

		NeuralNetDriver.HiddenLayerNeurons = config.HiddenLayers;

		MixNMojo.MutationRate = config.MutationRate;

		if (Application.isEditor) {
			TestRunner.MaxConcurrentTests = 3;
		}
	}

	public void DoShow() {
		GameObject.Find("LaunchUI").GetComponent<CanvasGroup>().alpha = 1;
		GameObject.Find("LaunchUI").GetComponent<CanvasGroup>().interactable = true;
		GameObject.Find("LaunchUI").GetComponent<CanvasGroup>().blocksRaycasts = true;
	}

	// Update is called once per frame
	void Update() {

	}
}
