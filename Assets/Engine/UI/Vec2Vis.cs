﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Vectrosity;

public class Vec2Vis : MonoBehaviour {

	public Vector2 value;
	RectTransform marker;
	Vector2 size;
	Vector2 parentPos;


	VectorLine lineHorz;
	VectorLine lineVert;


	// Start is called before the first frame update
	void Start() {
		Initialise();

	}

	void Initialise() {
		marker = transform.Find("Marker").GetComponent<RectTransform>();
		size = this.GetComponent<RectTransform>().sizeDelta / 2;
		parentPos = this.GetComponent<RectTransform>().position;
	}



	public void Hide() {
		if (lineHorz != null) {
			lineHorz.color = new Color(1, 1, 1, 0);
		}
		if (lineVert != null) {
			lineVert.color = new Color(1, 1, 1, 0);
		}
	}

	public void Show() {
		if (lineHorz != null) {
			lineHorz.color = new Color(1, 1, 1, .5f);
		}
		if (lineVert != null) {
			lineVert.color = new Color(1, 1, 1, .5f);
		}

	}

	// Update is called once per frame
	void Update() {
		var pos = new Vector2(
			Mathf.Clamp(value.x, -1, 1),
			Mathf.Clamp(value.y, -1, 1));
		marker.localPosition = Vector2.Scale(pos, size);

		if (lineHorz == null) {
			var points = new Vector2[] { Vector2.Scale(size, Vector2.left), Vector2.Scale(size, Vector2.right) };
			lineHorz = VectorLine.SetLine(new Color(1, 1, 1, 0), points);
		}
		lineHorz.points2[0] = new Vector2(size.x * -1, pos.y * size.y) + parentPos;
		lineHorz.points2[1] = new Vector2(size.x, pos.y * size.y) + parentPos;
		lineHorz.Draw();


		if (lineVert == null) {
			var points = new Vector2[] { Vector2.Scale(size, Vector2.left), Vector2.Scale(size, Vector2.right) };
			lineVert = VectorLine.SetLine(new Color(1, 1, 1, 0), points);
		}
		lineVert.points2[0] = new Vector2(pos.x * size.x, size.y) + parentPos;
		lineVert.points2[1] = new Vector2(pos.x * size.x, size.y * -1) + parentPos;
		lineVert.Draw();

		//Debug.LogFormat("{1} x {2} = {0}", Vector2.Scale(pos, size), pos, size);
	}
}
