﻿using DriveNet.Assets.Engine.Core.NeuralNetwork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class VehicleStatusText : MonoBehaviour {

	private GameObject vehicle = null;
	public void SelectedVehicleChanged() {
		vehicle = UIManager.GetCurrentVehicle();
		Initialise();
		Show();
	}

	public GameObject goal1Marker;
	private Vec2Vis goal1;
	public GameObject goal2Marker;
	private Vec2Vis goal2;
	public GameObject goal3Marker;
	private Vec2Vis goal3;

	

	public GameObject nearGoalPrefab;
	public GameObject midGoalPrefab;
	public GameObject farGoalPrefab;

	public GameObject steeringMarker;
	public GameObject throttleMarker;
	public GameObject speedMarker;
	public GameObject neuralNetworkVisualisation;
	//public TextMeshProUGUI bestLabel;

	// Update is called once per frame
	private TextMeshProUGUI speedLabel;


	private FloatVis steering;
	private FloatVis throttle;
	private FloatVis speed;

	public bool showGoals;

	private CanvasGroup cg;

	private void Hide() {
		if (cg.alpha > 0) {
			cg.alpha = 0f;
			cg.blocksRaycasts = false;
			goal1.Hide();
			goal2.Hide();
			goal3.Hide();
			neuralNetworkVisualisation.SetActive(false);
		}
	}
	private void Show() {
		cg.alpha = 1f;
		cg.blocksRaycasts = true;
		goal1.Show();
		goal2.Show();
		goal3.Show();
		if (nnVis.view != NeuralNet2Vis.ViewModes.Hidden) {
			neuralNetworkVisualisation.SetActive(true);
		}
	}

	private void Initialise() {
		if (goal1Marker == null) {
			throw new ArgumentException("Goal 1 Marker must be set!");
		} else {
			goal1 = goal1Marker.GetComponent<Vec2Vis>();
		}
		if (goal2Marker == null) {
			throw new ArgumentException("Goal 2 Marker must be set!");
		} else {
			goal2 = goal2Marker.GetComponent<Vec2Vis>();
		}
		if (goal3Marker == null) {
			throw new ArgumentException("Goal 2 Marker must be set!");
		} else {
			goal3 = goal3Marker.GetComponent<Vec2Vis>();
		}
		if (steeringMarker == null) {
			throw new ArgumentException("Steering Marker must be set!");
		} else {
			steering = steeringMarker.GetComponent<FloatVis>();
		}
		if (speedMarker == null) {
			throw new ArgumentException("Steering Marker must be set!");
		} else {
			speed = speedMarker.GetComponent<FloatVis>();
		}
		if (throttleMarker == null) {
			throw new ArgumentException("Throttle Marker must be set!");
		} else {
			throttle = throttleMarker.GetComponent<FloatVis>();
		}
		if (neuralNetworkVisualisation == null) {
			throw new ArgumentException("Neural Netowrk Vis Placeholder must be set!");
		} else {
			nnVis = neuralNetworkVisualisation.GetComponent<NeuralNet2Vis>();
		}
		cg = this.transform.parent.GetComponent<CanvasGroup>();
		if (vehicle == null) {
			Hide();
			return;
		}
		sensors = vehicle.GetComponent<SensorSuite>();
		if (sensors == null) {
			throw new InvalidOperationException("No functioning sensor suite found!");
		}
		goalProvider = vehicle.GetComponent<GoalProvider>();
		if (goalProvider == null) {
			throw new InvalidOperationException("No goal provider found!");
		}
		dummy = vehicle.GetComponent<TestDummy>();
		if (dummy == null) {
			throw new InvalidOperationException("No test dummy found!");
		}
		nnVis.network = (dummy.driver as NeuralNetDriver).RawNetwork;
		nnVis.Rebuild();




		if (nearMarker == null) {
			nearMarker = Instantiate(nearGoalPrefab);
			nearMarker.transform.parent = this.transform;
		}

		if (midMarker == null) {
			midMarker = Instantiate(midGoalPrefab);
			midMarker.transform.parent = this.transform;
		}


		if (farMarker == null) {
			farMarker = Instantiate(farGoalPrefab);
			farMarker.transform.parent = this.transform;
		}



	}




	SensorSuite sensors;
	GoalProvider goalProvider;
	TestDummy dummy;
	private NeuralNet2Vis nnVis;
	GameObject nearMarker;
	GameObject midMarker;
	GameObject farMarker;

	// Start is called before the first frame update
	void Start() {
		//label = this.transform.Find("Label").GetComponent<TextMeshProUGUI>();
		Initialise();
		var sl = speedMarker.GetComponent<FloatVis>();
		if (sl.labelObject != null) {
			speedLabel = sl.labelObject.GetComponent<TextMeshProUGUI>();
		}
	}

	//TextMeshProUGUI label;


	void Update() {

		if (Input.GetKeyDown(KeyCode.G)) {
			showGoals = !showGoals;
		}
		if (vehicle == null) {
			Initialise();
		} else {
			nearMarker.transform.position = sensors.nearTarget * (showGoals ? 1 : 0);
			midMarker.transform.position = sensors.midTarget * (showGoals ? 1 : 0);
			farMarker.transform.position = sensors.farTarget * (showGoals ? 1 : 0);

			goal1.value = goalProvider.goal1;
			goal2.value = goalProvider.goal2;
			goal3.value = goalProvider.goal3;
			throttle.value = dummy.controlOutput.y;
			steering.value = dummy.controlOutput.x;
			speed.value = (sensors.speed - 25) / 30f;
			if (speedLabel != null) {
				speedLabel.text = String.Format("{0:0.0m/s}", sensors.speed);
			}

			//var msg = new StringBuilder();
			//if (dummy.driver != null) {
			//	msg.AppendFormat("{0}\n", dummy.driver.GenomeName);
			//}
			//msg.AppendFormat("Top Speed: {0:0.0}\n", sensors.topSpeed);
			//msg.AppendFormat("Avg Speed: {0:0.0}\n", sensors.averageSpeed);
			//if (!sensors.onTrack) {
			//	msg.AppendFormat("*** OFF TRACK ***\n");
			//}
			//label.text = msg.ToString();
		}
	}

}
