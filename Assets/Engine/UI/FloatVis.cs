﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Vectrosity;

public class FloatVis : MonoBehaviour {

	public float value;
	public bool isVertical;
	public bool isBiDirectional = true;
	RectTransform marker;
	private Vector2 size;
	private Vector2 parentPos;
	public GameObject labelObject;
	private RectTransform label;





	// Start is called before the first frame update
	void Start() {
		Initialise();

	}

	void Initialise() {
		marker = transform.Find("Marker").GetComponent<RectTransform>();
		size = this.GetComponent<RectTransform>().sizeDelta / 2;
		parentPos = this.GetComponent<RectTransform>().position;
		if (labelObject != null) {
			label = labelObject.GetComponent<RectTransform>();
		}
	}



	// Update is called once per frame
	void Update() {
		float pos;
		if (isBiDirectional) {
			pos = Mathf.Clamp(value, -1, 1);
		} else {
			pos = Mathf.Clamp(value, 0, 1);
			pos = pos * 2 - 1;
		}

		if (isVertical) {
			marker.localPosition = new Vector2(0, pos * size.y);
			//if (label != null) {
			//	label.localPosition = new Vector2(label.localPosition.x, pos * size.y / 2);
			//	label.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, pos * size.y);
			//}
		} else {
			marker.localPosition = new Vector2(pos * size.x, 0);
			//if (label != null) {
			//	label.localPosition = new Vector2(pos * size.x, label.localPosition.y / 2);
			//	label.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, pos * size.x);
			//}
		}


		//marker.localPosition = Vector2.Scale(pos, size);
		//Debug.LogFormat("{1} x {2} = {0}", Vector2.Scale(pos, size), pos, size);
	}
}
