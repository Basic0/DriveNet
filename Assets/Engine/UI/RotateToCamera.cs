﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToCamera : MonoBehaviour {
	public Camera mainCamera;
	private RectTransform rectTransform;
	// Start is called before the first frame update
	void Start() {
		if (mainCamera == null) {
			mainCamera = Camera.main;
			
		}
		rectTransform = this.GetComponent<RectTransform>();
	}

	// Update is called once per frame
	void Update() {
		//rectTransform.LookAt(Vector3.Scale(mainCamera.localTransform.forward, new Vector3(1, 0, 1)));
		rectTransform.LookAt(rectTransform.position + Vector3.Scale(mainCamera.transform.forward, new Vector3(1, 0, 1)));
		//Debug.LogFormat("{0} -> {1}", mainCamera.transform.forward, rectTransform.rotation);

	}
}
