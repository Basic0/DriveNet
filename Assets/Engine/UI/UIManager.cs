﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DriveNet.Assets.Engine.Core;
using TMPro;
using System;

public class UIManager : MonoBehaviour {
	private const int fpsSmear = 10;
	public static GameObject currentVehicle;
	private FinishLine fl;
	//public static Vector3 currentFocus = new Vector3(760.3345f, 25.80274f, 526.9076f);
	public static Vector3 currentFocus = new Vector3(600, 200, 400);
	private bool flyInComplete = false;
	public static bool IsLerping { get; private set; }
	public UnityEvent selectedVehicleChanged;
	public UnityEvent flyInCompleteEvent;
	
	private TestRunner runner;
	
	public static GameObject GetCurrentVehicle() {
		return currentVehicle;
	}

	public void SwitchTo(GameObject vehicle) {
		if (vehicle != null) {
			if (vehicle != currentVehicle) {
				if (lerpCoroutine != null) {
					StopCoroutine(lerpCoroutine);
				}
				lerpCoroutine = StartCoroutine(SmoothLerp(currentFocus, vehicle));
			}
			currentVehicle = vehicle;
			selectedVehicleChanged.Invoke();
		}
	}


	private Coroutine lerpCoroutine;
	private float lerpTime = .5f;
	private IEnumerator SmoothLerp(Vector3 from, Vector3 to, bool triggerLaunch) {
		var duration = triggerLaunch ? 5f : 0.5f;
		IsLerping = true;
		float elapsed = 0;
		while (elapsed < duration) {
			currentFocus = Vector3.Lerp(from, to, (-Mathf.Cos(elapsed / duration * Mathf.PI) + 1) / 2);
			elapsed += Time.deltaTime;
			yield return null;
		}
		currentFocus = to;
		IsLerping = false;
		if (triggerLaunch) {
			flyInCompleteEvent.Invoke();
		}
	}
	private IEnumerator SmoothLerp(Vector3 from, GameObject to) {
		IsLerping = true;
		float elapsed = 0;
		while (elapsed < lerpTime) {
			currentFocus = Vector3.Lerp(from, to.transform.position, (-Mathf.Cos(elapsed / lerpTime * Mathf.PI) + 1) / 2);
			elapsed += Time.deltaTime;
			yield return null;
		}
		currentFocus = to.transform.position;
		IsLerping = false;
	}


	public void ViewLatest() {
		var tests = runner.ActiveTests();
		if (tests.Count > 0) {
			SwitchTo(tests.Last().Vehicle);
		}
	}


	private float fpsTime;
	public float Fps { get; private set; }
	void UpdateFps() {
		float fTime = 1 / Time.deltaTime;
		Fps = (Fps * (fpsSmear - 1) + fTime) / fpsSmear;
		fpsText.text = String.Format("{0:0.0} FPS", Fps);
	}


	public void SwitchIfNotActive() {
		if (currentVehicle != null
			&& currentVehicle.GetComponent<SensorSuite>() != null
			&& currentVehicle.GetComponent<SensorSuite>().isActiveAndEnabled) {
			return;
		}
		var tests = runner.ActiveTests();
		if (tests.Count > 0) {
			SwitchTo(tests.Last().Vehicle);
		}
	}

	private CanvasGroup fpsContainer;
	private TextMeshProUGUI fpsText;


	// Start is called before the first frame update
	void Start() {
		runner = GameObject.Find("TestRunner").GetComponent<TestRunner>();
		fl = GameObject.Find("FinishLine").GetComponent<FinishLine>();
		Application.targetFrameRate = -1;
		QualitySettings.vSyncCount = 0;

		fpsContainer = Resources.FindObjectsOfTypeAll<CanvasGroup>().Where(x => x.name == "Fps").First().GetComponent<CanvasGroup>();
		fpsText = Resources.FindObjectsOfTypeAll<TextMeshProUGUI>().Where(x => x.name == "FpsDisplay").First().GetComponent<TextMeshProUGUI>();
	}

	GameObject neuralNetVis;

	// Update is called once per frame
	void Update() {
		UpdateFps();
		if (neuralNetVis == null) {
			neuralNetVis = GameObject.Find("NNVis");
		}
		if (currentVehicle == null) {
			ViewLatest();
		}
		if (currentVehicle != null && !IsLerping) {
			currentFocus = currentVehicle.transform.position;

		}
		if (currentVehicle == null && lerpCoroutine == null) {

			lerpCoroutine = StartCoroutine(SmoothLerp(currentFocus, fl.spawnPoint, !flyInComplete));
			flyInComplete = true;
		}
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			ViewSkip(forward: true);
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			ViewSkip(forward: false);
		}
		if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.KeypadEnter)) {
			ViewBestScore();
		}
		if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.KeypadPeriod)) {
			ViewFastest();
		}
		if (Input.GetKeyDown(KeyCode.F)) {
			fpsContainer.gameObject.SetActive(!fpsContainer.gameObject.activeInHierarchy);
		}
		if (Input.GetKeyDown(KeyCode.N) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))) {
			if (neuralNetVis != null) {
				var wasActive = neuralNetVis.activeInHierarchy;
				neuralNetVis.SetActive(!wasActive);
				neuralNetVis.GetComponent<NeuralNet2Vis>().view = wasActive ? NeuralNet2Vis.ViewModes.Hidden : NeuralNet2Vis.ViewModes.Off;
			}
		}


		if (Input.GetKeyDown(KeyCode.K) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))) {
			foreach (var t in runner.ActiveTests()) {
				if (t.Vehicle != currentVehicle) {
					t.Sensors.HandleFailure("Killed");
				}
			}
		}
	}

	public void ViewBestScore() {
		var test = runner.ActiveTests().OrderByDescending(x => x.TestDummy.Score).FirstOrDefault();
		if (test != null) {
			//Debug.LogFormat("Jumping to vehicle with score of {0} (of {1})", test.TestDummy.Score, runner.ActiveTests().Count);
			SwitchTo(test.Vehicle);
		}
	}

	public void ViewFastest() {
		if (fl.CurrentSplitTime() != null) {
			var o = runner.ActiveTests().Where(x => x.Sensors.bestTrackPoint > (SensorSuite.TrackSegments / 10)).OrderBy(x => x.Sensors.splitVariance);
			var test = o.FirstOrDefault();
			if (test != null) {
				SwitchTo(test.Vehicle);
			}
		} else {
			ViewBestScore();
		}
	}

	private void ViewSkip(bool forward) {
		var tests = runner.ActiveTests();
		if (tests.Count > 0) {
			var match = tests.Select((t, i) => new { t, i }).SingleOrDefault(x => x.t.Vehicle == currentVehicle);
			int index;
			if (match == null) {
				index = 0;
			} else {
				index = match.i;
			}
			index += forward ? 1 : -1;
			index = ((index % tests.Count) + tests.Count) % tests.Count;
			SwitchTo(tests.ElementAt(index).Vehicle);
		}
	}


}
