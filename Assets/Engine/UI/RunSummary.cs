﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Vectrosity;
using UnityEngine.UI;

public class RunSummary : MonoBehaviour {
	const int HistorySize = 1024;
	const int Divisions = 10;
	private TestRunner tr;
	private int lastRun = 0;
	private float maxScore = 1;

	private RectTransform rt;
	private Canvas cnv;


	private TextMeshProUGUI highLabel;
	//private TextMeshProUGUI quintLabel;
	//private TextMeshProUGUI medianLabel;
	private TextMeshProUGUI avgLabel;


	VectorLine highLine;
	//VectorLine quintLine;
	//VectorLine medianLine;
	VectorLine avgLine;
	VectorLine[] decLine = new VectorLine[Divisions];

	public Color highSpeedColor = new Color(0, 1, .25f);
	public Color decSpeedColor = new Color(0.02741191f, 0.5714968f, 0.8301887f);
	//public Color quintSpeedColor = new Color(1, 0, 1);
	//public Color medianSpeedColor = Color.cyan;
	public Color averageSpeedColor = Color.yellow;

	private float[,] decScores = new float[HistorySize, Divisions];
	private float[] highScores = new float[HistorySize];
	private float[] avgScores = new float[HistorySize];
	//private float[] quintScores = new float[HistorySize];
	//private float[] medianScores = new float[HistorySize];
	void Start() {
		Initialise();
	}

	void Initialise() {
		rt = this.GetComponent<RectTransform>();
		cnv = GameObject.Find("UI").GetComponent<Canvas>();
		var trO = GameObject.Find("TestRunner");
		if (trO == null) {
			throw new InvalidProgramException("Testrunner couldn't be found in the scene!");
		}
		tr = trO.GetComponent<TestRunner>();

		highLabel = BuildLabel("Best", highSpeedColor);
		avgLabel = BuildLabel("Average", averageSpeedColor);
		MoveLabel(ref highLabel, -1000, false);
		MoveLabel(ref avgLabel, -1000, false);

		maxScore = highScores.Max();



		for (int j = 0; j < Divisions; j++) {
			var decPoints = new Vector2[HistorySize];
			
			decLine[j] = VectorLine.SetLine(
				Color.Lerp(decSpeedColor / 2, decSpeedColor, j / Divisions),
				decPoints);
		}


		var highPoints = new Vector2[HistorySize];
		highLine = VectorLine.SetLine(highSpeedColor, highPoints);
		var avgPoints = new Vector2[HistorySize];
		avgLine = VectorLine.SetLine(averageSpeedColor, avgPoints);






	}

	private TextMeshProUGUI BuildLabel(string label, Color color) {
		var go = new GameObject(string.Format("RunSummaryLabel.{0}", label));
		go.transform.parent = this.transform;
		var tmp = go.AddComponent<TextMeshProUGUI>();
		var rect = go.GetComponent<RectTransform>();
		rect.localPosition = new Vector3(0, 0, 0);
		rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 150);
		rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 18);
		tmp.fontSize = 14;
		tmp.alignment = TextAlignmentOptions.Right;
		tmp.faceColor = color;
		tmp.text = label;
		return tmp;
	}

	// Update is called once per frame
	void Update() {
		if (tr.reportManager.CurrentRun != lastRun) {
			lastRun = tr.reportManager.CurrentRun - 1;
			LogResults();
			DrawResults();
		}
	}

	void LogResults() {
		var stats = tr.reportManager.GetRunReport(lastRun);
		if (stats.Max > 0) {
			Debug.LogFormat("Parsing chart data for run {0}", lastRun);
			this.GetComponent<Image>().enabled = true;
			highScores[lastRun % HistorySize] = stats.Max;
			avgScores[lastRun % HistorySize] = stats.Average;
			for (int j = 0; j < Divisions; j++) {
				decScores[lastRun % HistorySize, j] = stats.Decile[j];
				
			}
			for (int i = (lastRun + 1) % HistorySize; i < HistorySize; i++) {
				highScores[i] = highScores[lastRun % HistorySize];
				avgScores[i] = avgScores[lastRun % HistorySize];
				for (int j = 0; j < Divisions; j++) {
					decScores[i, j] = decScores[lastRun % HistorySize, j];
				}
			}
			maxScore = Mathf.Max(maxScore, highScores[lastRun]);
		}
		lastRun = tr.reportManager.CurrentRun;
	}

	void DrawResults() {
		var pos = RectTransformUtility.PixelAdjustRect(rt, cnv);
		var scaleX = 1f / (lastRun % HistorySize) * pos.width;
		var scaleY = 1f / maxScore * RectTransformUtility.PixelAdjustRect(rt, cnv).height * .95f;
		var offsetX = rt.transform.position.x - (rt.sizeDelta.x / 2f);
		var offsetY = rt.transform.position.y - (rt.sizeDelta.y / 2f);
		///var offsetX = 1;rt.transform.position
		for (int i = 0; i < HistorySize; i++) {
			if (i > lastRun) {
				highLine.points2[i] = new Vector2(offsetX + lastRun * scaleX, offsetY + highScores[lastRun] * scaleY);
				//quintLine.points2[i] = new Vector2(offsetX + lastRun * scaleX, offsetY + quintScores[lastRun] * scaleY);
				//medianLine.points2[i] = new Vector2(offsetX + lastRun * scaleX, offsetY + medianScores[lastRun] * scaleY);
				avgLine.points2[i] = new Vector2(offsetX + lastRun * scaleX, offsetY + avgScores[lastRun] * scaleY);
				for (int j = 0; j < 10; j++) {
					decLine[j].points2[i] = new Vector2(offsetX + lastRun * scaleX, offsetY + decScores[lastRun, j] * scaleY);
				}


			} else {
				highLine.points2[i] = new Vector2(offsetX + i * scaleX, offsetY + highScores[i] * scaleY);
				//quintLine.points2[i] = new Vector2(offsetX + i * scaleX, offsetY + quintScores[i] * scaleY);
				//medianLine.points2[i] = new Vector2(offsetX + i * scaleX, offsetY + medianScores[i] * scaleY);
				avgLine.points2[i] = new Vector2(offsetX + i * scaleX, offsetY + avgScores[i] * scaleY);
				for (int j = 0; j < 10; j++) {
					decLine[j].points2[i] = new Vector2(offsetX + i * scaleX, offsetY + decScores[i, j] * scaleY); 
				}
			}
			//Debug.Log(avgLine.points2[i]);
		}
		highLine.Draw();
		//quintLine.Draw();
		//medianLine.Draw();
		for (int j = 0; j < 10; j++) {
			decLine[j].Draw();
		}
		avgLine.Draw();

		highLabel.text = String.Format("{0:#,##0} Best", highScores.Last());
		MoveLabel(ref highLabel, highScores[HistorySize - 1], false);
		//MoveLabel(ref quintLabel, quintScores[HistorySize - 1], true);

		//medianLabel.text = String.Format("{0:#,##0} Med.", medianScores.Last());
		//MoveLabel(ref medianLabel, medianScores[HistorySize - 1], false);

		avgLabel.text = String.Format("{0:#,##0} Avg.", avgScores.Last());
		MoveLabel(ref avgLabel, avgScores[HistorySize - 1], true);

	}

	private void MoveLabel(ref TextMeshProUGUI label, float vPos, bool aboveLine) {
		var labelRect = label.GetComponent<RectTransform>();
		var scaleY = 1f / (Mathf.Max(1, maxScore)) * RectTransformUtility.PixelAdjustRect(rt, cnv).height * .95f;
		var offsetX = rt.transform.position.x + (rt.sizeDelta.x / 2f) - (labelRect.sizeDelta.x) / 2f;
		var offsetY = rt.transform.position.y - (rt.sizeDelta.y / 2f);

		var pos = new Vector2(offsetX, offsetY + vPos * scaleY + (aboveLine ? 5 : -5));
		//Debug.LogFormat("Label {0}: {1} -> {2}", label.text, vPos, pos);

		labelRect.position = pos;
	}
}
