﻿using NeuronDotNet.Core;
using NeuronDotNet.Core.Backpropagation;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
//using Vectrosity;

public class NeuralNet2Vis : MonoBehaviour {
	public Sprite nodeImage;
	public Material synapseMaterial;
	public BackpropagationNetwork network;
	private string networkShapeFingerprint = String.Empty;
	private RectTransform parentPanelTransform;
	private List<Image> edges = new List<Image>();
	public Gradient HeatColors;

	private List<GameObject> nodes = new List<GameObject>();

	private List<int> nodeStructure = new List<int>();
	private Dictionary<int, int> nodeMap = new Dictionary<int, int>();
	private Dictionary<int, Vector2Int> nodePlacementMap = new Dictionary<int, Vector2Int>();
	private Dictionary<int, int> edgeMap = new Dictionary<int, int>();


	private List<string> inputs = new List<string> { "Speed", "Near X", "Near Y", "Mid X", "Mid Y", "Far X", "Far Y" };
	private List<string> outputs = new List<string> { "Steering", "Throttle" };
	// Start is called before the first frame update
	private bool hasLabels;
	private bool hasArt;

	public ViewModes view = ViewModes.All;

	public enum ViewModes {
		Off,
		Nodes,
		All,
		Hidden,
	}

	GameObject synapseHost;
	GameObject neuronHost;
	void Start() {
		parentPanelTransform = this.GetComponent<RectTransform>();

		synapseHost = new GameObject("Synapses");
		synapseHost.transform.parent = this.transform;
		synapseHost.transform.localPosition = Vector3.zero;
		synapseHost.AddComponent<CanvasGroup>().transform.localPosition = Vector2.zero;

		neuronHost = new GameObject("Neurons");
		neuronHost.transform.parent = this.transform;
		neuronHost.transform.localPosition = Vector3.zero;
		neuronHost.AddComponent<CanvasGroup>().transform.localPosition = Vector2.zero;

		Initialise();
	}


	private void MoveLabel(ref TextMeshProUGUI label, Vector2 pos, bool aboveLine = true) {

		var labelRect = label.GetComponent<RectTransform>();
		var offset = Pos2Screenspace(pos);

		//var pos = new Vector2(offsetX, offsetY + vPos * scaleY + (aboveLine ? 5 : -5));
		//Debug.LogFormat("Label {0}: {1} -> {2}", label.text, vPos, pos);
		offset += Vector2.up * (aboveLine ? 10 : -25);
		labelRect.localPosition = offset;
	}





	private TextMeshProUGUI BuildLabel(string label, Color color) {
		var go = new GameObject(string.Format("MNLabel.{0}", label));
		go.transform.parent = synapseHost.transform;
		var tmp = go.AddComponent<TextMeshProUGUI>();
		var rect = go.GetComponent<RectTransform>();
		rect.localPosition = new Vector3(0, 0, 0);
		rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 150);
		rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 30);
		tmp.fontSize = 12;
		tmp.alignment = TextAlignmentOptions.Top;

		tmp.faceColor = color;
		tmp.text = label;
		return tmp;
	}


	internal void Rebuild() {

		Initialise();
	}



	private const float nodeSize = 20;
	private Vector2 overallScale = new Vector2(1f, .8f);
	void Initialise() {
		//Debug.Log("Initialising NN Viz");


		if (network != null && this.gameObject.activeInHierarchy) {
			//Debug.Log("Initialising NN Viz: HAS NETWORK");
			nodeMap.Clear();
			nodeStructure.Clear();
			nodePlacementMap.Clear();


			List<ILayer> layerList = null;
			try {
				layerList = network.Layers.ToList();
			} catch (NullReferenceException) {
				/*Don't care... Race condition that only occurs when we catch a NN being set up. we'll get it next frame*/
			}

			if (layerList != null) {
				nodeStructure = layerList.Select(l => l.NeuronCount).ToList();

				if (!hasArt) {
					edges.ForEach(e => GameObject.Destroy(e));
					nodes.ForEach(n => GameObject.Destroy(n));
					edges.Clear();
					nodes.Clear();

				}

				int i = 0;
				int li = 0;
				foreach (var l in layerList) {
					int ni = 0;
					foreach (var n in l.Neurons) {
						nodePlacementMap[n.GetHashCode()] = new Vector2Int(li, ni);
						if (!hasArt) {
							var go = new GameObject(String.Format("Neuron.{0}-{1}", li, ni)); //Create the GameObject
							go.transform.parent = neuronHost.transform;
							var img = go.AddComponent<Image>(); //Add the Image Component script
							var rt = go.GetComponent<RectTransform>();
							img.sprite = nodeImage; //Set the Sprite of the Image Component on the new GameObject
							img.material = synapseMaterial;
							//rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 20);
							//rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 20);
							//rt.transform.parent = this.transform;
							rt.SetParent(parentPanelTransform);
							rt.anchoredPosition = Vector2.zero;
							var pos = Pos2Screenspace(GetPos(li, ni), centeredOrigin: false);
							rt.offsetMin = new Vector2(pos.x - nodeSize / 2, pos.y - nodeSize / 2);
							rt.offsetMax = new Vector2(pos.x + nodeSize / 2, pos.y + nodeSize / 2);
							go.SetActive(true);
							nodes.Add(go);
						}
						if (view != ViewModes.Off) {
							nodes[i].GetComponent<Image>().color = new Color(.1f, .1f, .1f, .5f);
						} else {
							nodes[i].GetComponent<Image>().color = new Color(1, 1, 1, 1);
						}
						nodeMap[n.GetHashCode()] = i++;
						ni++;
					}
					li++;
				}
				int j = 0;
				int ci = 0;

				foreach (var c in network.Connectors) {
					int si = 0;
					foreach (var s in c.Synapses) {
						if (!hasArt) {
							var from = Pos2Screenspace(GetPos(nodePlacementMap[s.SourceNeuron.GetHashCode()]));
							var to = Pos2Screenspace(GetPos(nodePlacementMap[s.TargetNeuron.GetHashCode()]));


							//var line = VectorLine.SetLine(new Color(1, 1, 1, (float)s.Weight),
							//	Pos2Screenspace(from),
							//	Pos2Screenspace(to));


							var line = new GameObject(String.Format("Synapse {0}-{1}", ci, si));
							line.transform.parent = synapseHost.transform;
							var img = line.AddComponent<Image>();
							var irt = img.GetComponent<RectTransform>();
							irt.anchorMin = Vector2.zero;
							irt.anchorMax = Vector2.zero;
							irt.pivot = new Vector2(0, .5f);
							img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (to - from).magnitude);
							img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 3);
							irt.localPosition = from;
							irt.Rotate(new Vector3(0, 0, Vector2.SignedAngle(Vector2.right, to - from)));
							edges.Add(img);
						}
						if (view == ViewModes.Off) {
							edges[j].color = new Color(1, 1, 1, (float)s.Weight);
						} else {
							edges[j].color = new Color(.4f, .4f, .4f, (float)s.Weight);
						}
						edgeMap[s.GetHashCode()] = j++;
						si++;
					}
					ci++;
				}

				if (!hasLabels) {
					for (int k = 0; k < inputs.Count(); k++) {
						var lbl = BuildLabel(inputs[k], Color.white);
						var pos = GetPos(0, k);
						MoveLabel(ref lbl, pos);
					}
					for (int k = 0; k < outputs.Count(); k++) {
						var lbl = BuildLabel(outputs[k], Color.white);
						var pos = GetPos(nodeStructure.Count() - 1, k);
						MoveLabel(ref lbl, pos, false);
					}
					hasLabels = true;
				}
				hasArt = true;
			}
		}
		//parentPos = this.GetComponent<RectTransform>().position;
	}


	private Vector2 GetPos(Vector2Int location) {
		return GetPos(location.x, location.y);
	}
	private Vector2 GetPos(int layer, int index) {
		return new Vector2(
			(1 + index) / (float)(nodeStructure[layer] + 1), 1 - (layer / (float)(nodeStructure.Count - 1)));
	}


	private Vector2 Pos2Screenspace(Vector2 position, bool centeredOrigin = true) {

		Vector2 ret;
		if (centeredOrigin) {
			ret = -parentPanelTransform.sizeDelta
			+ Vector2.up * parentPanelTransform.sizeDelta.y
			+ overallScale * position * parentPanelTransform.sizeDelta
			+ (Vector2.one - overallScale) * parentPanelTransform.sizeDelta / 2;
		} else {
			ret = -parentPanelTransform.sizeDelta / 2
			+ overallScale * position * parentPanelTransform.sizeDelta
			+ (Vector2.one - overallScale) * parentPanelTransform.sizeDelta / 2;
		}


		//Debug.LogFormat("NNN LAYOUT:\n{0}", JsonConvert.SerializeObject(new {
		//	parentPanelTransformPosition = parentPanelTransform.position,
		//	parentPanelTransformSizeDelta = parentPanelTransform.sizeDelta + Vector2.up * parentPanelTransform.sizeDelta.y,
		//	OverallScale = overallScale,
		//	Position = position,
		//	Result = ret
		//}, Formatting.Indented));
		return ret;
	}




	void Update() {

		if (Input.GetKeyDown(KeyCode.N)) {
			ViewModes[] modes = (ViewModes[])Enum.GetValues(typeof(ViewModes));
			view = modes[((int)view + 1) % modes.Length];

			// Don't allow hidden here. That state is managed externally. So shoot me.
			if (view == ViewModes.Hidden) {
				view = modes[((int)view + 1) % modes.Length];
			}
			Initialise();
			Debug.LogFormat("NN Visualisation: {0}", view);
		}

		if (view != ViewModes.Off && view != ViewModes.Hidden && nodeMap.Count > 0 && nodes.Count > 0) {
			List<ILayer> layerList = null;
			try {
				layerList = network.Layers.ToList();
			} catch (NullReferenceException) {
				//Just give up and go home
				nodeMap.Values.ToList().ForEach(x => nodes[x].GetComponent<Image>().color = Color.black);
			}
			if (layerList != null) {
				if (view != ViewModes.Off) {
					int i = 0;
					int li = 0;
					foreach (var l in layerList) {
						foreach (var n in l.Neurons) {
							var node = nodes[i++];
							node.GetComponent<Image>().color = BuildColor(li == 0 ? n.Input : n.Output, 1);
						}
						li++;
					}
				}
				if (view == ViewModes.All) {
					int j = 0;
					foreach (var c in network.Connectors) {
						foreach (var s in c.Synapses) {
							var edge = edges[j++];
							edge.color = BuildColor(s.SourceNeuron.Output, s.Weight);
						}
					}
				}
			}
		}
	}

	private Color BuildColor(double value, double alpha = 1) {
		float identity = (float)(value / 2.0 + .5);
		var c = HeatColors.Evaluate(identity);
		return new Color(
			c.r,
			c.g,
			c.b,
			(float)alpha);
	}





	//private Color BuildColor(double value, double alpha = 1) {
	//	float identity = (float)(value / 2.0 + .5);
	//	float intensity = Mathf.Clamp((float)(Math.Abs(value) * 2 - 1), 0, 1);

	//	return new Color(
	//		identity + intensity,
	//		value > 0 ? intensity : 0,
	//		(1 - identity) + (value < 0 ? -intensity : intensity),
	//		(float)alpha);

	//}
}
