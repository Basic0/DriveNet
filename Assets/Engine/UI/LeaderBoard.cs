﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System;

public class LeaderBoard : MonoBehaviour {
	private const int positions = 10;
	private const int height = 17;
	private const int initialY = 80;

	// Start is called before the first frame update

	private TextMeshProUGUI[] slots;
	private TestRunner tr;
	void Start() {
		Initialise();
	}

	void Initialise() {
		var trO= GameObject.Find("TestRunner");
		if (trO == null) {
			throw new InvalidProgramException("Testrunner couldn't be found in the scene!");
		}

		tr = trO.GetComponent<TestRunner>();
		slots = new TextMeshProUGUI[positions];
		var width = this.GetComponent<RectTransform>().sizeDelta.x - 10;
		for (int i = 0; i < positions; i++) {
			var go = new GameObject(string.Format("LeaderBoard.Slot.{0}", i + 1));
			go.transform.parent = this.transform;
			var tmp = go.AddComponent<TextMeshProUGUI>();
			var rect = go.GetComponent<RectTransform>();
			slots[i] = tmp;
			rect.localPosition = new Vector3(0, initialY - i * height, 0);
			rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
			rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
			tmp.fontSize = height;
			tmp.text = i.ToString();
			tmp.enableAutoSizing = true;
			tmp.fontSizeMin = 8;
		}
	}

	// Update is called once per frame
	float timeSinceRefresh;
	void Update() {
		timeSinceRefresh += Time.deltaTime;
		if (timeSinceRefresh >= 1) {
			timeSinceRefresh = 0;
			var rank = tr.reportManager.GetRunReport(tr.reportManager.CurrentRun).TestReports.Take(positions).ToArray();
			for (int i = 0; i < positions; i++) {
				if (i < rank.Length) {
					slots[i].text = String.Format("{0,9:#,##0} | {1}:{2} | {3}",
						rank[i].Score,
						rank[i].Finished ? String.Format("{0:00}", rank[i].Time.Minutes) : "--",
						rank[i].Finished ? String.Format("{0:00}", rank[i].Time.Seconds) : "--",
						rank[i].GenomeName);
					slots[i].color = new Color(rank[i].Colour.X, rank[i].Colour.Y, rank[i].Colour.Z);
				} else {
					slots[i].text = String.Format(new string('~', 29), i + 1);
					slots[i].color = Color.gray;
				}
			}
		}
	}
}
