﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class TestRunnerStatus : MonoBehaviour {
	// Start is called before the first frame update
	public GameObject testRunner;
	public GameObject subLabel;
	private TestRunner tr;
	private TestPlanner tp;
	private TextMeshProUGUI label;
	private TextMeshProUGUI winnerLabel;
	void Start() {
		if (testRunner == null) {
			throw new ArgumentException("TestRunner must be specified");
		}
		tr = testRunner.GetComponent<TestRunner>();
		tp = testRunner.GetComponent<TestPlanner>();
		label = this.transform.Find("Label").GetComponent<TextMeshProUGUI>();
		winnerLabel = subLabel.GetComponent<TextMeshProUGUI>();
	}

	// Update is called once per frame
	int timeSlice;
	void Update() {
		timeSlice++;
		if (timeSlice >= 9) {
			timeSlice = 0;
			var msg = new StringBuilder();
			var runTime = tr.RunTime;
			int finished = tr.reportManager.GetRunReport(tr.reportManager.CurrentRun).TestReports.Count;
			msg.AppendFormat(
				"{0}{2:0}% of Run {1:#,##0} [{9}], {3} elapsed\n{4:#,##0} ready, {5}/{10} running{11}, {6:#,##0} finished\n{7:#,##0} of {8:#,##0} genotypes generated",
				tr.IsHalted ? "## HALTED [H] " : string.Empty,
				tr.reportManager.CurrentRun,
				100 * finished / tp.CurrentRoundSize,
				String.Format("{0:00}:{1:00}:{2:00}", runTime.Hours, runTime.Minutes, runTime.Seconds),
				tr.GetDriverQueueLength(),
				tr.ActiveTests().Count,
				finished,
				tp.QueuedThisRound,
				tp.CurrentRoundSize,
				FinishLine.ReverseDirection ? "Rev" : "Fwd",
				TestRunner.MaxConcurrentTests,
				TestRunner.QuickSpawn ? " [Fast]" : String.Empty);
			label.text = msg.ToString();

			if (tr.reportManager.best != null) {
				winnerLabel.text = String.Format(
					"Run {0}: {1}\n{2:#,##0.00} points\n{3} in {6:00}:{7:00}.{8:000}\n{4:0.00}m/s Top\n{5:0.00}m/s Average",
					tr.reportManager.bestRun,
					tr.reportManager.best.GenomeName,
					tr.reportManager.best.Score,
					tr.reportManager.best.Finished ? "Finished" : String.Format("{0:#,##0} segments", tr.reportManager.best.Details.Segments),
					tr.reportManager.best.Details.TopSpeed,
					tr.reportManager.best.Details.AverageSpeed,
					tr.reportManager.best.Time.Minutes,
					tr.reportManager.best.Time.Seconds,
					tr.reportManager.best.Time.Milliseconds);
				var col = tr.reportManager.best.Colour;
				winnerLabel.color = new Color(col.X, col.Y, col.Z);
			} else {
				winnerLabel.text = "Waiting for best driver...";
			}
		}
	}
}
