﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworkController : MonoBehaviour {

	public float roundEndDuration = 5;
	public ParticleSystem[] victoryParticles;
	public ParticleSystem[] roundEndParticles;
	private Coroutine cleanupWorker;
	private int victoryIndex;
	// Start is called before the first frame update
	void Start() {
		foreach (var p in victoryParticles) {
			p.Stop();
		}
		foreach (var p in roundEndParticles) {
			p.Stop();
			var m = p.main;
			m.playOnAwake = false;
			var e = p.emission;
			e.enabled = false;
		}
	}

	// Update is called once per frame
	void Update() {

		if (Input.GetKeyDown(KeyCode.Keypad0)) {
			EndRound();
		}

		if (Input.GetKeyDown(KeyCode.Keypad1)) {
			Celebrate(Color.red);
		}
		if (Input.GetKeyDown(KeyCode.Keypad2)) {
			Celebrate(Color.green);
		}
		if (Input.GetKeyDown(KeyCode.Keypad3)) {
			Celebrate(Color.blue);
		}
		if (Input.GetKeyDown(KeyCode.Keypad4)) {
			Celebrate(new Color(Random.value, Random.value, Random.value));
		}
	}


	public void EndRound() {
		if (cleanupWorker != null) {
			StopCoroutine(cleanupWorker);
		}
		cleanupWorker = StartCoroutine(EndRoundCelebration());
	}

	private IEnumerator EndRoundCelebration() {
		Debug.Log("Starting end of round celebration");
		foreach (var p in roundEndParticles) {
			var m = p.main;
			m.playOnAwake = true;
			var e = p.emission;
			e.enabled = true;
			p.Play();
		}
		yield return new WaitForSeconds(roundEndDuration);
		foreach (var p in roundEndParticles) {
			p.Stop();
			var m = p.main;
			m.playOnAwake = false;
			var e = p.emission;
			e.enabled = false;
		}
	}

	public void Celebrate(Color color) {
		victoryIndex++;
		victoryIndex = victoryIndex % victoryParticles.Length;
		var p = victoryParticles[victoryIndex];
		var main = p.main;
		main.startColor = color;
		p.Play();
	}
}
