﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMapIcon : MonoBehaviour {
	// Start is called before the first frame update

	private CameraDefault camDef;
	void Start() {
		camDef = this.transform.parent.GetComponent<CameraDefault>();
	}

	// Update is called once per frame
	void Update() {
		this.transform.position = camDef.GetLookAt() + Vector3.up * 20;
		this.transform.rotation = Quaternion.Euler(-90, 0, 0);
	}
}
