﻿using DriveNet.Assets.Engine.Core;
using DriveNet.Assets.Engine.Core.NeuralNetwork;
using DriveNet.Assets.Engine.Core.Reference;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TestPlanner : MonoBehaviour {

	[DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
	private static extern int StrCmpLogicalW(string psz1, string psz2);

	private AutoResetEvent requiredCountChanged = new AutoResetEvent(false);
	public bool isLoading { get; private set; } = true;

	private GameObject lp;
	private TextMeshProUGUI lpt;

	private ReportManager reportManager;
	private FinishLine fl;
	private TestRunner testRunner;
	public static int roundSize = 1000;
	public static int winnerCount = 15;
	public static bool includeTheBest = false;
	public static bool includeTheWeakest = false;
	public static bool loadHistory = false;
	volatile private int spawnedCount = 0;

	public int QueuedThisRound { get { return spawnedCount; } }


	public int CurrentRoundSize {
		get {
			return (reportManager.CurrentRun != 0 ? roundSize : roundSize * 2);
		}
	}

	public sealed class NaturalStringComparer : IComparer<string> {
		public int Compare(string a, string b) {
			return StrCmpLogicalW(a, b);
		}
	}

	private static ConcurrentStack<Action> dispatchQueue = new ConcurrentStack<Action>();
	public sealed class NaturalFileInfoNameComparer : IComparer<FileInfo> {
		public int Compare(FileInfo a, FileInfo b) {
			return StrCmpLogicalW(a.Name, b.Name);
		}
	}

	public static void Dispatch(Action callback) {
		dispatchQueue.Push(callback);
	}


	void Start() {
		lp = GameObject.Find("LoadingPane").gameObject;
		lpt = GameObject.Find("LoadingText").GetComponent<TextMeshProUGUI>();
		if (Application.isEditor) {
			roundSize = 100;
		}
		Initialise();
	}
	private void Initialise() {
		fl = GameObject.Find("FinishLine").GetComponent<FinishLine>();
		testRunner = this.GetComponent<TestRunner>();
		if (testRunner == null) {
			throw new InvalidProgramException("Test planner must be attached to the same GameObject as the TestRunner");
		}
		reportManager = testRunner.reportManager;


		var t = new Thread(() => SpawnWorker());
		t.IsBackground = true;
		t.Name = "TestPlanner.DriverSpawner";
		t.Start();
	}

	private void LoadCheckpoint() {
		string dir = reportManager.FolderPath;
		string filePattern = String.Format(reportManager.FilePattern, NeuralNetDriver.GenomeFormatFingerprint, "*");
		List<string> files = Directory
			.GetFiles(
				dir,
				filePattern)
			.OrderBy(x => x, new NaturalStringComparer())
			.ToList();
		UnityEngine.Debug.LogFormat("Scanned {0} for files matching the pattern {1} and found {2}",
			dir, filePattern, files.Count());
		//if (Application.isEditor && files.Count() > 0) {
		//	files = new List<string> { files.Last() };
		//}
		int i = 0;
		var serializer = new JsonSerializer();
		foreach (var runFile in files) {
			i++;
			int run = int.Parse(new string(runFile.ToCharArray().Where(c => char.IsNumber(c)).ToArray()));
			TestPlanner.Dispatch(() => {
				lpt.text = String.Format(
						"DriveNet\nParsing run {0:#,##0}\n{1:0}% complete",
						run,
						100f * i / files.Count());
			});

			UnityEngine.Debug.LogFormat("Loading run {0} from {1}", run, runFile);

			using (FileStream originalFileStream = File.Open(runFile, FileMode.Open)) {
				using (DeflateStream decompressionStream = new DeflateStream(originalFileStream, CompressionMode.Decompress)) {
					using (StreamReader reader = new StreamReader(decompressionStream)) {
						using (JsonTextReader jsonReader = new JsonTextReader(reader)) {
							var runReport = serializer.Deserialize<RunReport>(jsonReader);
							if (runReport != null) {
								runReport.TestReports.ForEach(x => x.Config = (x.Config as JObject).ToObject<NeuralGenome>());
								reportManager.BulkLoad(run, runReport);
								UnityEngine.Debug.LogFormat("Run {0} Best Candidate:\n{1}", run, JsonConvert.SerializeObject(runReport.TestReports.First(), Formatting.Indented));
							} else {
								UnityEngine.Debug.LogFormat("Error parsing run {0:#,##0}", run);
								TestPlanner.Dispatch(() => {
									lpt.text = String.Format(
											"DriveNet\nError parsing run {0:#,##0}",
											run);
								});
							}
						}
					}
				}
			}
			spawnedCount = CurrentRoundSize;
		}
		StartNextRound();
	}


	void Update() {

		Action callback;
		if (dispatchQueue.TryPop(out callback)) {
			//Just one per frame for smoothness... Change the IF to a WHILE to clear down
			callback.Invoke();
		}
		if (Input.GetKeyDown(KeyCode.Return)) {
			if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
				RunBest(10);
			} else {
				RunBest(1);
			}
		}

		if (Input.GetKeyDown(KeyCode.Backspace)) {
			RunGreatestHits();
		}

	}


	private void StartNextRound() {
		var reports = reportManager
			.GetRunReport(reportManager.CurrentRun)
			.TestReports
			.OrderByDescending(x => x.Score);
		if (reports.Count() > 0) {
			if (!isLoading) {
				Dispatch(() => {
					fl.fireworks.EndRound();
				});
			}
			UnityEngine.Debug.LogFormat("Closing round {0}", reportManager.CurrentRun);
			if (reports.Count() < 2) {
				UnityEngine.Debug.LogFormat("Only retrieved {0} reports for run {1}", reports.Count(), reportManager.CurrentRun);
			}

			var candidates = reports
				.Take(winnerCount)
				.Select(x => x.Config as NeuralGenome).ToList();

			if (includeTheWeakest) {
				candidates.AddRange(
					reports
					.Reverse()
					.Take(2)
					.Select(x => x.Config as NeuralGenome));
			}

			if (reportManager.best != null && includeTheBest) {
				candidates.Add(reportManager.best.Config as NeuralGenome);
			}

			if (testRunner.RunTime.TotalSeconds > 2) {
				TestPlanner.Dispatch(() => {
					lp.SetActive(true);
					lpt.text = String.Format(
							"Run {0} complete in {1:00}:{2:00}\n{3:#,##0} genomes selected\n{4:#,##0.00} Best\n{5:#,##0.00} Average",
							reportManager.CurrentRun,
							testRunner.RunTime.Minutes,
							testRunner.RunTime.Seconds,
							candidates.Count,
							reports.Max(x => x.Score),
							reports.Average(x => x.Score));
					var t = new Thread(() => {
						Thread.Sleep(10000);
						TestPlanner.Dispatch(() => {
							lp.SetActive(false);
						});
					});
					t.Name = "TestPlanner.UICleanup";
					t.IsBackground = true;
					t.Start();
				});
			}



			if (!FinishLine.ReverseDirection && reports.First().Score > fl.fwdScore) {
				fl.fwdScore = reports.First().Score;
				var split = reports.First().Split;
				TestPlanner.Dispatch(() => {
					fl.splitTime = split;
					fl.splitTime[fl.goal] = TimeSpan.Zero;
					UnityEngine.Debug.LogFormat("Lap Split set to: {0}", String.Join(", ", fl.splitTime.Select(x => x.TotalSeconds)));
				});
			}
			if (FinishLine.ReverseDirection && reports.First().Score > fl.revScore) {
				fl.revScore = reports.First().Score;
				var split = reports.First().Split;
				TestPlanner.Dispatch(() => {
					fl.revSplitTime = split;
					fl.revSplitTime[fl.goal] = TimeSpan.Zero;
					UnityEngine.Debug.LogFormat("Lap Split [Reverse] set to: {0}", String.Join(", ", fl.revSplitTime.Select(x => x.TotalSeconds)));
				});
			}
			geneMachine = (new NeuralNetSplicer()).Splice(candidates);
			reportManager.StartRun();
			spawnedCount = 0;
		}
	}



	private void SpawnWorker() {
		// Give the game a moment to finish loading in before we start hammering the CPU
		Thread.Sleep(1000);
		if (loadHistory) {
			LoadCheckpoint();
		}
		TestPlanner.Dispatch(() => {
			lp.SetActive(false);
		});
		isLoading = false;


		while (true) {
			//var tests = testRunner.reportManager.GetRunReport(testRunner.reportManager.CurrentRun);
			//UnityEngine.Debug.LogFormat("{0} complete, ", tests.Count, CurrentRoundSize, testRunner.ActiveTests().Count);
			if (spawnedCount >= CurrentRoundSize &&
				testRunner.ActiveTests().Count == 0) {
				UnityEngine.Debug.Log("End of round detected");
				StartNextRound();

			}
			//UnityEngine.Debug.LogFormat("Checking if we need to spawn new tests... {0} < {1}", spawnedCount, requiredCount);
			while (spawnedCount < CurrentRoundSize) {
				//UnityEngine.Debug.LogFormat("SUBChecking if we need to spawn new tests... {0} < {1}", spawnedCount, requiredCount);
				if (testRunner.GetDriverQueueLength() < Mathf.Max(50, TestRunner.MaxConcurrentTests * 2)) {
					AddTest();
				} else {
					// Wait for space to become available, or 1 secs.
					testRunner.carSpawned.WaitOne(1000);
				}
			}
			//UnityEngine.Debug.LogFormat("Think we're done {0} < {1}: {2}", spawnedCount, requiredCount, testRunner.GetDriverQueueLength());

			//Wait until we're told we need to add more. Check every second, _Just in case_
			requiredCountChanged.WaitOne(1000);
		}
	}


	private System.Random rnd = new System.Random();
	private IEnumerable<NeuralGenome> geneMachine;

	public void AddTest() {
		spawnedCount++;
		IDriver driver;
		if (geneMachine is null) {
			driver = new NeuralNetDriver { Config = null };
		} else {
			driver = new NeuralNetDriver {
				Config = geneMachine.First()
			};
			//UnityEngine.Debug.LogFormat("Spawning {0}", driver.GenomeName);

		}
		testRunner.AddCandidate(driver);
	}

	public void RunGreatestHits() {
		for (int i = reportManager.CurrentRun - 1; i >= 0; i--) {
			var conf = reportManager.GetRunReport(i).TestReports.First().Config as NeuralGenome;
			var driver = new NeuralNetDriver {
				Config = conf
			};
			driver.ForceName(String.Format("Round {0} winner", i));
			testRunner.AddCandidate(driver, asGhost: true);
		}
	}
	public void RunBest(int cloneCount) {
		var conf = reportManager.best.Config as NeuralGenome;
		for (int i = cloneCount; i > 0; i--) {
			var driver = new NeuralNetDriver {
				Config = conf
			};
			driver.ForceName(String.Format("Fittest [Clone {0}]", i));
			testRunner.AddCandidate(driver, asGhost: true);
		}
	}
}
