﻿using DriveNet.Assets.Engine.Core;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

public class CarPool : MonoBehaviour {
	// Start is called before the first frame update

	public GameObject vehicleTemplate;

	private readonly ConcurrentBag<GameObject> garage = new ConcurrentBag<GameObject>();
	private FinishLine finishLine;
	public static bool LightsOn;

	void Start() {
		var finishLineGO = GameObject.Find("FinishLine");
		if (finishLineGO == null) {
			throw new InvalidProgramException("Unable to locate an object in the scene called 'FinishLine'");
		}

		finishLine = finishLineGO.GetComponent<FinishLine>();
		if (finishLine == null) {
			throw new InvalidProgramException("Finish Line object doesn't contain a FinishLine script");
		}
		//PreWarm(100);
	}


	private void PreWarm(int count) {
		var q = new Queue<GameObject>();
		for (int i = 0; i < count; i++) {
			q.Enqueue(GetVehicle());
		}
		while (q.Count > 0) {
			ReturnVehicle(q.Dequeue());
		}
	}

	private IDriver comaDriver = new ComaPatient();
	private int maxCount = 0;
	public void ReturnVehicle(GameObject vehicle) {
		var testDummy = vehicle.GetComponent<TestDummy>();

		// Suspend the driver worker thread
		testDummy.SetDriverActive(false);

		//Inject a coma patient to release the existing driver
		testDummy.SetDriver(comaDriver, Color.white);

		vehicle.GetComponent<SensorSuite>().DoReset();
		//vehicle.GetComponent<VehicleSpeedo>().DoReset();
		vehicle.transform.position = Vector3.zero;
		vehicle.SetActive(false);
		garage.Add(vehicle);
		if (maxCount < garage.Count) {
			maxCount = garage.Count;
		}
	}
	private GameObject GetVehicle() {
		GameObject ret;
		if (garage.TryTake(out ret)) {
			return ret;
		} else {
			ret = Instantiate(vehicleTemplate, Vector3.zero, Quaternion.identity, this.transform);
			return ret;
		}
	}

	internal GameObject GetVehicle(Vector3 spawnPoint, Quaternion spawnOrientation) {
		var vehicle = GetVehicle();
		vehicle.transform.SetPositionAndRotation(spawnPoint, spawnOrientation);
		vehicle.GetComponent<SensorSuite>().DoReset();
		//vehicle.GetComponent<VehicleSpeedo>().DoReset();
		vehicle.SetActive(true);
		var lights = vehicle.transform.Find("Body").GetComponentsInChildren<Light>();
		foreach (var l in lights) {
			l.gameObject.SetActive(LightsOn);
		}
		return vehicle;
	}
}
