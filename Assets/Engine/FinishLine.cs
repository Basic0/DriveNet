﻿using DriveNet.Assets.Engine.Core;
using EasyRoads3Dv3;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour {
	public GameObject finishPolePrefab;
	public static bool ReverseDirection = false;
	public static bool FlipFlopDirection = true;
	public static bool AdvanceGoal = false;
	public int goal = 0;
	private ERRoad road;
	Vector3[] centerSpline;

	[HideInInspector] public float fwdScore;
	[HideInInspector] public float revScore;


	[HideInInspector] public Vector3 spawnPoint;
	[HideInInspector] public Quaternion spawnOrientation;
	[HideInInspector] public TimeSpan[] splitTime;

	[HideInInspector] public Quaternion revSpawnOrientation;
	[HideInInspector] public TimeSpan[] revSplitTime;
	// Start is called before the first frame update


	[HideInInspector] public FireworkController fireworks;

	public GameObject finishObj;
	void Start() {

		ERRoadNetwork network;
		try {
			network = new ERRoadNetwork();
		} catch (Exception e) {
			throw new ArgumentException("Couldn't load road network", e);
		}
		var roads = network.GetRoads();
		if (roads.Length == 0) {
			throw new ArgumentException("No roads!");
		}
		road = roads[0];
		centerSpline = road.GetSplinePointsCenter();
		finishObj = BuildFinishline();
		fireworks = finishObj.GetComponent<FireworkController>();
		if (!fireworks) {
			Debug.LogWarning("Failed to find fireworks controller!");
		}
		SetTheScene();

	}

	private void SetTheScene() {
		var center = road.GetSplinePointsCenter();
		spawnPoint = GetSplinePoint(ref center, goal) + Vector3.up * .5f;
		var nextPoint = GetSplinePoint(ref center, goal + 1) + Vector3.up * .5f;
		var prevPoint = GetSplinePoint(ref center, goal - 1) + Vector3.up * .5f;
		spawnOrientation = Quaternion.LookRotation(nextPoint - spawnPoint);
		revSpawnOrientation = Quaternion.LookRotation(prevPoint - spawnPoint);

		finishObj.transform.rotation = spawnOrientation;
		finishObj.transform.position = GetSplinePoint(ref centerSpline, goal);
		
		Debug.LogFormat("Finish line: {0}",
			JsonConvert.SerializeObject(new {
				SplinePoint = goal,
				Direction = (ReverseDirection ? "Reverse" : "Forward"),
				SpawnPosition = spawnPoint,
				SpawnOrientation = new {
					TopDownAngle = spawnOrientation.eulerAngles.y,
					Vector = (spawnOrientation * Vector3.forward).normalized
				},
			}, Formatting.Indented));
	}

	public Quaternion CurrentSpawnOrientation() {
		return ReverseDirection ? revSpawnOrientation : spawnOrientation;
	}
	public TimeSpan[] CurrentSplitTime() {
		return ReverseDirection ? revSplitTime : splitTime;
	}

	private GameObject BuildFinishline() {
		var ret = Instantiate(finishPolePrefab);
		ret.transform.parent = this.transform;
		return ret;
	}

	private Vector3 GetSplinePoint(ref Vector3[] spline, int splineIndex) {
		return spline[((splineIndex % spline.Length) + spline.Length) % spline.Length];
	}

	// Update is called once per frame
	int lastRun = 0;

	void Update() {
		if (lastRun != ReportManager.StaticCurrentRun) {
			lastRun = ReportManager.StaticCurrentRun;
			if (FlipFlopDirection) {
				ReverseDirection = !ReverseDirection;
			}
			if (AdvanceGoal) {
				goal += (int)(centerSpline.Length / 100f);
				goal = goal % centerSpline.Length;
				if (goal < 3 || goal > centerSpline.Length - 3) {
					goal = 3;
				}
			}
			SetTheScene();
		}
	}
}
