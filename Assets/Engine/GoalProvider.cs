﻿using EasyRoads3Dv3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GoalProvider : MonoBehaviour {
	[HideInInspector] public Vector2 goal1;
	[HideInInspector] public Vector2 goal2;
	[HideInInspector] public Vector2 goal3;

	SensorSuite sensors;
	void Start() {
		sensors = this.GetComponent<SensorSuite>();
		if (sensors == null) {
			throw new InvalidOperationException("No functioning sensor suite found!");
		}

	}

	private void Update() {
		setGoal(out goal1, sensors.nearTarget);
		setGoal(out goal2, sensors.midTarget);
		setGoal(out goal3, sensors.farTarget);

	}

	private void setGoal(out Vector2 goal, Vector3 target) {
		//var closestApproach = FindNearestPointOnLine(this.transform.position, this.transform.forward, target);
		var flatTarget = Vector3.Scale(target - transform.position, new Vector3(1, 0, 1));
		var flatPos = Vector3.Scale(transform.forward, new Vector3(1, 0, 1)) * flatTarget.magnitude;

		//var vertPos = Vector3.Scale(transform.forward, new Vector3(1, 0, 1));
		//var vertTarget = Vector3.Scale(target, new Vector3(0, 1, 1));


		var horz = Vector3.SignedAngle(flatPos, flatTarget, Vector3.up) / 90f;
		var vert = (target.y - transform.position.y) / 10f;
		goal = new Vector2(
			Mathf.Clamp(horz, -1, 1),
			Mathf.Clamp(vert, -1, 1));

	}


	private Vector3 FindNearestPointOnLine(Vector3 origin, Vector3 direction, Vector3 point) {
		direction.Normalize();
		Vector3 lhs = point - origin;

		float dotP = Vector3.Dot(lhs, direction);
		return origin + direction * dotP;
	}
	
	private float Horizontal(Vector3 to) {
		//var relative = (to - this.transform.position).normalized;
		// this.transform.forward
		return Vec2OverheadAngle(to);
	}

	private float Vec2OverheadAngle(Vector3 point) {
		var q = Quaternion.Euler(point);
		return q.eulerAngles.y;
	}

	private float Vertical(Vector3 to) {
		return 0;
	}


}