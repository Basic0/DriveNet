﻿using DriveNet.Assets.Engine.Core;
using DriveNet.Assets.Engine.Core.NeuralNetwork;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;

public class TestDummy : MonoBehaviour {

	[HideInInspector] public Vector2 controlOutput;
	[HideInInspector] public Color colour;

	private float minThrottle = 0;
	private float maxThrottle = 0;
	public bool IsGhost;

	//private object config;
	public float Score { get; set; }

	public bool manualOverride = false;
	public float maxTorque = 1000f;
	public float maxSteerAngle = 45f;
	public Transform t_CenterOfMass;
	public Transform[] wheelMesh = new Transform[4];
	public WheelCollider[] wheelCollider = new WheelCollider[4];
	private volatile bool shouldDrive = true;
	private AutoResetEvent shouldDriveChanged = new AutoResetEvent(false);
	private AutoResetEvent frameAdvanced = new AutoResetEvent(false);

	public Color colorFailed = new Color(1, .5f, .5f);

	private Thread driverThread;

	public IDriver driver;
	Rigidbody rb;
	SensorSuite sensors;
	GoalProvider goals;
	TextMeshPro statusLabel;
	void Start() {
		sensors = this.GetComponent<SensorSuite>();
		if (sensors == null) {
			throw new InvalidOperationException("No functioning sensor suite found!");
		}
		goals = this.GetComponent<GoalProvider>();
		if (goals == null) {
			throw new InvalidOperationException("No goal provider found!");
		}

		statusLabel = this.GetComponentInChildren<TextMeshPro>();
		if (statusLabel == null) {
			throw new InvalidOperationException("No status label TextMeshPro object found on this vehicle!");
		}

		var slMeshRenderer = statusLabel.GetComponent<MeshRenderer>();
		slMeshRenderer.motionVectorGenerationMode = MotionVectorGenerationMode.Camera;
		slMeshRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;


		//driver = new NeuralNetDriver();
		if (driver == null) {
			driver = new ComaPatient();
		}

		//GetComponent<VehicleSpeedo>().speedoColor = colour;


		rb = GetComponent<Rigidbody>();
		rb.centerOfMass = Vector3.Scale(t_CenterOfMass.localPosition, transform.localScale);

		driverThread = new Thread(() => DriverExecutor());
		driverThread.IsBackground = true;
		driverThread.Name = "TestDummy.Driver";
		driverThread.Start();
	}

	public void SetDriverActive(bool active) {
		shouldDrive = active;
		shouldDriveChanged.Set();
	}

	private void DriverExecutor() {
		while (true) {
			if (shouldDrive) {
				//Debug.LogFormat("{0} -> {1}", sensors.speed, Mathf.Clamp((sensors.speed - 20) / 30f, -1, 1));
				var speed = Mathf.Clamp((sensors.speed - 25) / 30f, -1, 1); // Give a nice scale so -1 to 1 represents -5 to +55 m/s
				var output = driver.Step(speed, goals.goal1, goals.goal2, goals.goal3);
				controlOutput = new Vector2(Mathf.Clamp(output.x, -1, 1), Mathf.Clamp(output.y, -1, 1));
				frameAdvanced.WaitOne(1000);
			} else {
				controlOutput = Vector2.zero;
				shouldDriveChanged.WaitOne(1000);
			}
		}
	}

	internal TestReport BuildReport() {
		UpdateScore();
		var ret = new TestReport {
			GenomeName = driver.GenomeName,
			Score = this.Score,
			Time = sensors.lapTime,
			Colour = new System.Numerics.Vector3(this.colour.r, this.colour.g, this.colour.b),
			Split = this.sensors.splitTimes,
			Finished = this.sensors.hasFinished,
			Details = new TestReport.DetailedReport {
				AverageSpeed = (float)sensors.averageSpeed,
				TopSpeed = sensors.topSpeed,
				Tachometer = null, // sensors.speedHistory.Take(sensors.speedHistoryIndex).ToArray(),
				Segments = sensors.bestTrackPoint
			}
		};
		return ret;
	}

	private void UpdateScore() {
		float newScore = sensors.bestTrackPoint;
		//newScore += (Mathf.Abs(minThrottle) + Mathf.Abs(maxThrottle)) * 25;
		//newScore += (float)sensors.topSpeed;
		if (sensors.hasFinished) {
			newScore += (float)(sensors.maxTestDuration - sensors.lapTime.TotalSeconds) * 100;
			newScore += sensors.topSpeed;
		} else {
			newScore += sensors.topSpeed / 20;
			newScore += (float)sensors.averageSpeed / 20;
		}
		Score = newScore;
	}

	public void SetDriver(IDriver driver, Color driverColor) {
		this.colour = driverColor;
		this.driver = driver;
		if (sensors != null) {
			lastSegment = -1;
			sensors.DoReset();
		}
		transform.Find("CenterOfMass").Find("Label").gameObject.GetComponent<TextMeshPro>().faceColor = colour;

		MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
		propertyBlock.SetColor("_Color", colour);
		var mkr = transform.Find("MapMarker");
		mkr.GetComponent<MeshRenderer>().SetPropertyBlock(propertyBlock);
		mkr.transform.localScale = Vector3.one * 50;
	}

	// Update is called once per frame
	void FixedUpdate() {
		UpdateMeshPosition();
		minThrottle = Mathf.Min(minThrottle, controlOutput.y);
		maxThrottle = Mathf.Max(maxThrottle, controlOutput.y);
		float steer;
		float torque;
		if (manualOverride) {
			steer = Input.GetAxis("Horizontal") * maxSteerAngle;
			torque = Input.GetAxis("Vertical") * maxTorque;
		} else {
			steer = controlOutput.x * maxSteerAngle;
			torque = Mathf.Clamp(
				controlOutput.y,
				sensors.speed > 0 ? -1 : -.75f,
				1) * maxTorque;
		}

		if (wheelCollider[0] != null) {
			wheelCollider[0].steerAngle = steer;
			wheelCollider[1].steerAngle = steer;

			for (int i = 0; i < 4; i++) {
				wheelCollider[i].motorTorque = torque;
			}
		}
	}

	private float updateIn = 0;
	private int lastSegment = -1;
	public void Update() {
		frameAdvanced.Set();
		if (sensors.IsRunning()) {
			UpdateScore();
			if (Input.GetKeyDown(KeyCode.K) && !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))) {
				UnityEngine.Debug.LogFormat("Driver failed: Killed by user");
				sensors.HandleFailure("Killed");
			}
		}
		updateIn -= Time.deltaTime;
		if (updateIn < 0 || lastSegment != sensors.referenceIndex) {
			lastSegment = sensors.referenceIndex;
			updateIn = 1f;
			if (sensors.lapTime != null) {
				//string millis = sensors.lapTime.Milliseconds.ToString();
				if (sensors.hasFailed) {
					statusLabel.fontSize = 40;
					statusLabel.text = String.Format("{0:#,##0}\n{1}", Score, sensors.failureReason);
				} else {
					if (sensors.hasFinished) {
						statusLabel.fontSize = 40;
						statusLabel.text = String.Format("{0:#,##0.0}", Score);
					} else {
						statusLabel.fontSize = 25;
						statusLabel.text = String.Format(
							"{3:#,##0}{5}\n{2}{0:00}:{1:00}\n{4}",
							sensors.lapTime.Minutes,
							sensors.lapTime.Seconds,
							(!FinishLine.AdvanceGoal && sensors.splitVariance.HasValue) ? String.Format("{0:+0.00;-0.00}\n", sensors.splitVariance.Value) : "",
							Score,
							driver.GenomeName,
							IsGhost ? " [Ghost]" : String.Empty);
					}
				}

				if (sensors.hasStarted && sensors.hasFailed) {
					statusLabel.faceColor = colorFailed;
				}
			}
		}
	}


	private GameObject[] wheelCollection;
	public void UpdateMeshPosition() {
		if (wheelCollider[0] != null) {
			for (int i = 0; i < 4; i++) {
				Quaternion quat;
				Vector3 pos;

				wheelCollider[i].GetWorldPose(out pos, out quat);

				wheelMesh[i].position = pos;
				wheelMesh[i].rotation = quat;
			}
		}
	}
}
