﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DriveNet.Assets.Engine.Core {
	public interface IDriver {
		Vector2 Step(float speed, Vector2 goal1, Vector2 goal2, Vector2 goal3);
		string GenomeName { get; }
		string GenomeFormatFingerprint { get; }
	object Config { get; set; }
	}
}
