﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DriveNet.Assets.Engine.Core {
	public class RunReport {
		public List<TestReport> TestReports { get; set; } = new List<TestReport>();
		public float Max { get; set; }
		public float Average { get; set; }
		public float[] Decile { get; set; }
	}
}
