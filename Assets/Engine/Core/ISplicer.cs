﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveNet.Assets.Engine.Core {
	public interface ISplicer<T> {
		IEnumerable<T> Splice(List<T> candidates);
	}
}
