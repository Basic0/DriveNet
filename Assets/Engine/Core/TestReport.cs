﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DriveNet.Assets.Engine.Core {
	public class TestReport {
		public string GenomeName { get; set; }
		public TimeSpan Time { get; set; }
		public Vector3 Colour { get; set; }
		public Object Config { get; set; }
		public float Score { get; set; }
		public bool Finished { get; set; }
		public DetailedReport Details { get; set; }
		public TimeSpan[] Split { get; set; }


		public class DetailedReport {
			public float TopSpeed { get; set; }
			public float AverageSpeed { get; set; }
			public int Segments { get; set; }
			public float[] Tachometer { get; set; }
		}
	}
}
