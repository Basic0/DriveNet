﻿using System;
using System.Collections.Generic;

namespace DriveNet.Assets.Engine.Core {
	internal class RunConfig {
		public Guid Id { get; internal set; }
		public int GenePoolSize { get; set; }
		public int SurvivalSize { get; internal set; }
		public float MutationRate { get; set; }
		public int KeepFittest { get; set; }
		public List<int> HiddenLayers { get; set; }
		public bool IncludeHistoricalBest { get; set; }
		public bool IncludeRoundWorst { get; set; }
		public bool LoadHistory { get; set; }
		public int ConcurrentVehicles { get; set; }
		public bool StartForward { get; internal set; }
		public bool AdvanceGoal { get; internal set; }
		public bool AlternateDirection { get; internal set; }
	}
}