﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DriveNet.Assets.Engine.Core {
	public class ReportManager {

		public string FolderPath { get; private set; }
		public string FilePattern { get; private set; }

		private Dictionary<int, RunReport> reports = new Dictionary<int, RunReport>();
		private static int runIndex = -1;
		private int firstWriteIndex = 0;

		public TestReport best;
		public int bestRun;



		public ReportManager() {
			FolderPath = System.IO.Path.Combine(
				System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				"DriveNet");
			if (!Directory.Exists(FolderPath)) {
				Directory.CreateDirectory(FolderPath);
			}
			FilePattern = "{0}.Gen{1}.json.deflate";
		}
		
		public int ReportsCollected { get; private set; }
		public int CurrentRun { get { return runIndex; } }
		public static int StaticCurrentRun { get { return runIndex; } }
		//public List<TestReport> GetRunReports(int run) {
		//	if (reports.ContainsKey(run)) {
		//		return reports[run].OrderByDescending(x => x.Score).ToList();
		//	} else {
		//		return new List<TestReport>();
		//	}
		//}
		public RunReport GetRunReport(int run) {
			if (reports.ContainsKey(run)) {
				return reports[run];
			} else {
				return new RunReport();
			}
		}

		public void StartRun() {
			if (runIndex >= 0) {
				Flush(runIndex);
			}
			runIndex += 1;
			reports.Add(runIndex, new RunReport());
		}

		public void SaveReport(TestReport report) {
			ReportsCollected += 1;
			var rep = reports[runIndex].TestReports;
			rep.Add(report);
			reports[runIndex].TestReports = rep.OrderByDescending(x => x.Score).ToList();
			reports[runIndex].Max = rep.Max(x => x.Score);
			reports[runIndex].Average = rep.Average(x => x.Score);
			reports[runIndex].Decile = Enumerable
				.Range(1, 10)
				.Select(
					d => {
						var candidate = rep
							.Skip((int)(d / 10f * rep.Count))
							.FirstOrDefault();
						if (candidate != null) {
							return candidate.Score;
						} else {
							return 0;
						}
					})
				.ToArray();
		}


		public void BulkLoad(int run, RunReport runReport) {
			var reportStack = runReport.TestReports;
			reports[run] = runReport;
			if (best == null || reports[run].Max > best.Score) {
				best = reports[run].TestReports.First();
				bestRun = run;
			}

			if (run >= runIndex) {
				Debug.LogFormat("Setting run to {0}", run);
				runIndex = run;
			}
			if (run >= firstWriteIndex) {
				firstWriteIndex = run + 1;
			}
		}

		private JsonSerializer serializer = new JsonSerializer { MaxDepth = 1 };
		private void Flush(int run) {
			if (run >= firstWriteIndex) {
				string path = System.IO.Path.Combine(
					FolderPath,
					String.Format(FilePattern, NeuralNetwork.NeuralNetDriver.GenomeFormatFingerprint, run));
				var report = reports[run];
				if (best == null || report.Max > best.Score) {
					best = report.TestReports.First();
					bestRun = CurrentRun;
				}

				report.TestReports = report
					.TestReports
					.Take(100)
					.Union(report.TestReports.OrderBy(x => x.Score).Take(2)).ToList();

				report.TestReports.Skip(1).ToList().ForEach(x => {
					x.Split = null;
				});


				if (!UnityEngine.Application.isEditor) {

					using (FileStream compressedFileStream = File.Open(path, FileMode.Create)) {
						using (DeflateStream compressionStream = new DeflateStream(compressedFileStream, CompressionMode.Compress)) {
							using (StreamWriter writer = new StreamWriter(compressionStream)) {
								using (JsonTextWriter jsonWriter = new JsonTextWriter(writer)) {
									JsonSerializer ser = new JsonSerializer();
									ser.Serialize(jsonWriter, report);
									jsonWriter.Flush();
								}
							}
						}
					}
				}
			}
		}
	}
}
