﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DriveNet.Assets.Engine.Core.Reference {
	public class ReferenceDriver : IDriver {
		public object Config { get { return config; } set { config = value as ReferenceDriverConfig; } }
		private ReferenceDriverConfig config;

		public string GenomeFormatFingerprint => "ReferenceDriver";


		public class ReferenceDriverConfig {
			public float NearSteer { get; set; }
			public float FarSteer { get; set; }
			public float TopSpeed { get; set; }
			public float SafeSpeed { get; set; }
			public float SteeringTightness { get; set; } = 8;
		}


		private string _name;
		public string GenomeName {
			get {
				if (String.IsNullOrEmpty(_name)) {
					var fullName = String.Format("{0:0.00}-{1:0.00}@{2:0.00}|{3:0.0}/{4:0.0}",
						config.SafeSpeed,
						config.TopSpeed,
						config.SteeringTightness,
						config.NearSteer,
						config.FarSteer);
					var md5 = MD5.Create();
					var hash = md5.ComputeHash(new MemoryStream(Encoding.UTF8.GetBytes(fullName)));
					_name = "R." + fullName;// "R." + string.Concat(Array.ConvertAll(hash, b => b.ToString("X2")).Take(8));
					Debug.LogFormat("Name {0}, derived from:\n{1}", _name, fullName);
				}
				return _name;
			}
		}

		//public Vector2 Step(Vector2 goal1, Vector2 goal2, float speed) {
		//	var targetSpeed = Mathf.Clamp(
		//		50 - Mathf.Abs(goal1.x) * 500 - Mathf.Abs(goal2.x) * 1200f
		//		, 18, 60);
		//	return new Vector2(goal1.x * 8, (targetSpeed - speed) / 2);
		//}
		public Vector2 Step(float speed, Vector2 goal1, Vector2 goal2, Vector2 goal3) {
			var targetSpeed = Mathf.Clamp(
				config.TopSpeed - (Mathf.Abs(goal1.x) * config.NearSteer * 2500 - Mathf.Abs(goal3.x) * config.FarSteer * 2500)
				, config.SafeSpeed, config.TopSpeed);
			var throt = (targetSpeed - speed) * 5;
			return new Vector2(goal1.x * config.SteeringTightness * 10, throt < 0 ? throt * 5 : throt);
		}
	}
}
