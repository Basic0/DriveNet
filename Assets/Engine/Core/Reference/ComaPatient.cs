﻿using UnityEngine;

namespace DriveNet.Assets.Engine.Core {
	public class ComaPatient : IDriver {
		public string GenomeName => "Coma Patient";
		public object Config { get; set; }

		public string GenomeFormatFingerprint => throw new System.NotImplementedException("You can't save a coma patient!");

		public Vector2 Step(float speed, Vector2 goal1, Vector2 goal2, Vector2 goal3) {
			return Vector2.down * speed / 5f;
		}
	}
}
