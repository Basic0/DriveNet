﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveNet.Assets.Engine.Core.Reference {
	public class ReferenceSplicer : ISplicer<ReferenceDriver.ReferenceDriverConfig> {
		public IEnumerable<ReferenceDriver.ReferenceDriverConfig> Splice(List<ReferenceDriver.ReferenceDriverConfig> candidates) {
			return new ChildGenerator(candidates);
		}

		public class ChildGenerator : IEnumerable<ReferenceDriver.ReferenceDriverConfig> {
			private Random rnd = new Random();
			private List<ReferenceDriver.ReferenceDriverConfig> parents;
			public ChildGenerator(List<ReferenceDriver.ReferenceDriverConfig> candidates) {
				if (candidates.Count < 2) {
					throw new ArgumentException("Gene pool must contain at least 2 genomes");
				}
				parents = candidates;
			}

			private Tuple<ReferenceDriver.ReferenceDriverConfig, ReferenceDriver.ReferenceDriverConfig> GetMatingPair() {
				var pair = parents.OrderBy(x => rnd.NextDouble()).Take(2);
				return new Tuple<ReferenceDriver.ReferenceDriverConfig, ReferenceDriver.ReferenceDriverConfig>(
					pair.ElementAt(0), pair.ElementAt(1));
			}

			private ReferenceDriver.ReferenceDriverConfig Generate() {
				var pair = GetMatingPair();
				var child = new ReferenceDriver.ReferenceDriverConfig {
					NearSteer = MixNMojo.Merge(pair.Item1.NearSteer, pair.Item2.NearSteer, min: 0),
					FarSteer = MixNMojo.Merge(pair.Item1.FarSteer, pair.Item2.FarSteer, min: 0),
					SafeSpeed = MixNMojo.Merge(pair.Item1.SafeSpeed, pair.Item2.SafeSpeed, min: 0),
					SteeringTightness = MixNMojo.Merge(pair.Item1.SteeringTightness, pair.Item2.SteeringTightness, min: 0),
					TopSpeed = MixNMojo.Merge(pair.Item1.TopSpeed, pair.Item2.TopSpeed, min: 0),
				};
				return child;
			}


			public IEnumerator<ReferenceDriver.ReferenceDriverConfig> GetEnumerator() {
				yield return Generate();
			}

			IEnumerator IEnumerable.GetEnumerator() {
				return GetEnumerator();
			}
		}




	}
}
