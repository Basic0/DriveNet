﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DriveNet.Assets.Engine.Core.NeuralNetwork {
	public class NeuralNetSplicer : ISplicer<NeuralGenome> {
		public IEnumerable<NeuralGenome> Splice(List<NeuralGenome> candidates) {
			return new ChildGenerator(candidates);
		}

		public class ChildGenerator : IEnumerable<NeuralGenome> {
			private Random rnd = new Random();
			private List<NeuralGenome> parents;

			public ChildGenerator(List<NeuralGenome> candidates) {
				if (candidates.Count < 2) {
					throw new ArgumentException("Gene pool must contain at least 2 genomes");
				}
				UnityEngine.Debug.LogFormat("Creating new Gene Splicer using {0} candidates", candidates.Count);
				parents = candidates;
			}

			private Tuple<NeuralGenome, NeuralGenome> GetMatingPair() {
				var pair = parents.OrderBy(x => rnd.NextDouble()).Take(2);
				return new Tuple<NeuralGenome, NeuralGenome>(
					pair.ElementAt(0), pair.ElementAt(1));
			}


			IEnumerator IEnumerable.GetEnumerator() {
				return GetEnumerator();
			}
			public IEnumerator<NeuralGenome> GetEnumerator() {
				yield return Generate();
			}

			private NeuralGenome Generate() {
				var pair = GetMatingPair();
				var tmp = pair.Item1.Weights;
				var ret = new NeuralGenome {
					Weights = new List<List<Tuple<double, double>>>()
				};

				for (int i = 0; i < tmp.Count; i++) {
					var layer = new List<Tuple<double, double>>();
					for (int j = 0; j < tmp[i].Count; j++) {
						layer.Add(MixNMojo.Merge(pair.Item1.Weights[i][j], pair.Item2.Weights[i][j]));
					}
					ret.Weights.Add(layer);
				}

				return ret;
			}
		}
	}
}
