﻿using NeuronDotNet.Core;
using NeuronDotNet.Core.Backpropagation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace DriveNet.Assets.Engine.Core.NeuralNetwork {
	public class NeuralNetDriver : IDriver {
		public static List<int> HiddenLayerNeurons { get; set; } = new List<int> { 10, 10, 0 };


		BackpropagationNetwork network;

		public BackpropagationNetwork RawNetwork { get { return network; } }

		static string IntToChar(int index) {
			index -= 1;
			if (index < 26) {
				//Return the alphabet
				return char.ConvertFromUtf32(65 + index);
			} else {
				//Get creative
				return char.ConvertFromUtf32(188 - 26 + index);
			}
		}

		public object Config {
			get {
				if (config == null) {
					config = new NeuralGenome {
						Weights = GetNetworkWeights()
					};
				}
				return config;
			}
			set {
				var val = value as NeuralGenome;
				if (val != null) {
					_name = String.Empty;
					SetNetworkWeights(val.Weights);
				}
				config = null; // force regenerate from network (source of truth) rather than trusting, in case the entwork clamps
			}
		}
		private NeuralGenome config;
		private string _name;
		public string GenomeName {
			get {
				if (network == null) {
					return "<No Network>";
				} else {
					if (String.IsNullOrEmpty(_name)) {
						var fullName = String.Join("\n", (Config as NeuralGenome).Weights.Select(c =>
							String.Join(", ", c.Select(s =>
								String.Format("{0:0.00}-{1:0.00}", s.Item1, s.Item2)
							))
						));
						var md5 = MD5.Create();
						var hash = md5.ComputeHash(new MemoryStream(Encoding.UTF8.GetBytes(fullName)));
						_name = "N." + string.Concat(Array.ConvertAll(hash, b => b.ToString("X2")).Take(8));
						//Debug.LogFormat("Name {0}, derived from:\n{1}", _name, fullName);
					}
					return _name;
				}
			}
		}

		public static string GenomeFormatFingerprint => String.Join("-", HiddenLayerNeurons.Select(x => IntToChar(x)));

		string IDriver.GenomeFormatFingerprint => GenomeFormatFingerprint;

		//public string GenomeFormatFingerprintImpl GenomeFormatFingerprint => GenomeFormatFingerprint;

		public NeuralNetDriver() {
			Initialise();
		}
		public void ForceName(string newName) {
			_name = newName;
		}

		public Vector2 Step(float speed, Vector2 goal1, Vector2 goal2, Vector2 goal3) {
			var result = network.Run(new double[7] { speed, goal1.x, goal1.y, goal2.x, goal2.y, goal3.x, goal3.y });
			return new Vector2((float)result[0], (float)result[1]);
		}

		private static System.Random rnd = new System.Random();

		private void Initialise() {
			TanhLayer inputLayer = new TanhLayer(7);

			List<TanhLayer> layers = new List<TanhLayer> { inputLayer };
			for (int i = 0; i < HiddenLayerNeurons.Count(); i++) {
				layers.Add(new TanhLayer(HiddenLayerNeurons[i]));
			}

			TanhLayer outputLayer = new TanhLayer(2);
			layers.Add(outputLayer);
			//Debug.LogFormat("Spawning new NN Driver with {0} total layers", layers.Count);

			if (layers.Count < 3) {
				throw new ArgumentException("Must be at least one hidden layer!");
			}


			List<BackpropagationConnector> connectors = new List<BackpropagationConnector>();
			for (int i = 0; i < layers.Count() - 1; i++) {
				connectors.Add(new BackpropagationConnector(layers[i], layers[i + 1]));

			}
			//Debug.LogFormat("Established {0} layer connectors", connectors.Count);


			//BackpropagationConnector connector = new BackpropagationConnector(inputLayer, hiddenLayer1);
			//BackpropagationConnector connector2 = new BackpropagationConnector(hiddenLayer1, hiddenLayer2);
			//BackpropagationConnector connector3 = new BackpropagationConnector(hiddenLayer2, hiddenLayer3);
			//BackpropagationConnector connector5 = new BackpropagationConnector(hiddenLayer3, outputLayer);

			network = new BackpropagationNetwork(inputLayer, outputLayer);
			var data = network.Connectors.Select(c => c.Synapses.Select(s => new Tuple<double, double>(rnd.NextDouble() * 2 - 1, rnd.NextDouble() * 2 - 1)).ToList()).ToList();
			network.Initialize();
			SetNetworkWeights(data);
		}

		private void SetNetworkWeights(List<List<Tuple<double, double>>> weights) {
			// Setup the network's weights.
			int i = 0;
			foreach (BackpropagationConnector connector in network.Connectors) {
				int j = 0;
				foreach (BackpropagationSynapse synapse in connector.Synapses) {
					synapse.Weight = weights[i][j].Item1;
					synapse.SourceNeuron.Bias = weights[i][j].Item2;

					j++;
				}
				i++;
			}
		}

		private List<List<Tuple<double, double>>> GetNetworkWeights() {
			var ret = new List<List<Tuple<double, double>>>();
			foreach (BackpropagationConnector connector in network.Connectors) {
				var nodes = new List<Tuple<double, double>>();
				foreach (BackpropagationSynapse synapse in connector.Synapses) {
					nodes.Add(new Tuple<double, double>(synapse.Weight, synapse.SourceNeuron.Bias));
				}
				ret.Add(nodes);
			}
			return ret;
		}
	}
}
