﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveNet.Assets.Engine.Core.NeuralNetwork {
	public class NeuralGenome : IComparable<NeuralGenome> {
		public List<List<Tuple<double, double>>> Weights { get; set; }

		public int CompareTo(NeuralGenome other) {
			return String.Compare(Flatten(), other.Flatten());
		}

		private string Flatten() {
			return String.Concat(Weights.SelectMany(l => l.SelectMany(s => String.Format("{0}'{1}|", s.Item1, s.Item2))));
		}
	}
}
