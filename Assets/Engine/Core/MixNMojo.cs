﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DriveNet.Assets.Engine.Core {
	public class MixNMojo {
		public static float MutationRate = 0.015f;
		private static Random rnd = new Random();
		public static float Merge(
			float a,
			float b,
			float min = -1,
			float max = 1) {
			if (rnd.NextDouble() <= MutationRate) {
				return (float)(rnd.NextDouble() * (max - min) + min);
			} else {
				if (rnd.NextDouble() >= 0.5) {
					return a;
				} else {
					return b;
				}
			}
		}
		public static Tuple<double, double> Merge(
			Tuple<double, double> a,
			Tuple<double, double> b,
			float min = -1,
			float max = 1) {
			if (rnd.NextDouble() <= MutationRate) {
				// Mutant!
				return new Tuple<double, double>(
					(rnd.NextDouble() * (max - min) + min),
					(rnd.NextDouble() * (max - min) + min));
			} else {
				if (rnd.NextDouble() >= 0.5) {
					return a;
				} else {
					return b;
				}
			}
		}
		public static Vector2 Merge(
			Vector2 a,
			Vector2 b,
			float min = -1,
			float max = 1) {
			if (rnd.NextDouble() <= MutationRate) {
				float newX, newY;
				//Mutate X?
				if (rnd.NextDouble() <= .5) {
					//No, copy from random parent
					if (rnd.NextDouble() <= .5) {
						newX = a.X;
					} else {
						newX = b.X;
					}
				} else {
					//Yes ...
					newX = (float)(rnd.NextDouble() * (max - min) + min);
				}

				//Mutate Y?
				if (rnd.NextDouble() <= .5) {
					//No, copy from random parent
					if (rnd.NextDouble() <= .5) {
						newY = a.Y;
					} else {
						newY = b.Y;
					}
				} else {
					// Yes
					newY = (float)(rnd.NextDouble() * (max - min) + min);
				}

				return new Vector2(newX, newY);

			} else {
				if (rnd.NextDouble() < 0.5) {
					return a;
				} else {
					return b;
				}
			}
		}
	}
}
