﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DriveNet.Assets.Engine.Core {
	[Serializable]
	public class TestCase  {
		public GameObject Vehicle { get; set; }
		public IDriver Driver { get; set; }
		public SensorSuite Sensors { get; set; }
		public TestDummy TestDummy { get; set; }
		public bool IsGhost { get; set; }
		public Color Colour { get; set; }

		public bool IsActive() {
			if (Sensors == null) {
				return true;
			}
			return !(Sensors.hasFailed || Sensors.hasFinished);
		}

		public void Kill() {
			GameObject.Destroy(Vehicle);

		}
	}
}
