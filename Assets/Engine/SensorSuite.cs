﻿using EasyRoads3Dv3;
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Runtime.CompilerServices;
using System.Linq;

public class SensorSuite : MonoBehaviour {
	public static bool hasSquawked;
	// Start is called before the first frame update
	public float maxTestDuration = 120;
	public float maxIdleTime = 5;

	[HideInInspector] private float timeSinceProgressMade;
	[HideInInspector] public bool onTrack;
	[HideInInspector] public bool hasStarted;
	[HideInInspector] public bool hasFinished;
	[HideInInspector] public bool hasFailed;
	[HideInInspector] public int referenceIndex = -1;
	[HideInInspector] public Vector3 roadCenter;
	[HideInInspector] public Vector3 nearTarget;
	[HideInInspector] public Vector3 midTarget;
	[HideInInspector] public Vector3 farTarget;
	[HideInInspector] public float centerDistance;
	[HideInInspector] public float variance;
	[HideInInspector] public float speed;
	[HideInInspector] public float speedToGoal;
	[HideInInspector] public FinishLine finishLine;
	//[HideInInspector] public GameObject band;

	private Vector3 lastLocation;

	private int wrongDirectionCount = 0;
	private List<int> pointHistory = new List<int>();

	private float lastGoalDistance;

	Vector2 t1Vel = Vector2.zero;
	Vector2 t2Vel = Vector2.zero;
	private Transform centerOfMass;

	internal void DoReset() {
		timeSinceProgressMade = 0;
		hasStarted = false;
		hasFinished = false;
		hasFailed = false;
		referenceIndex = -1;
		speed = 0;
		speedToGoal = 0;
		lastLocation = Vector3.zero;
		lastGoalDistance = 0;
		t1Vel = Vector3.zero;
		t2Vel = Vector3.zero;
		failureReason = String.Empty;

		lapTime = TimeSpan.Zero;
		topSpeed = 0;
		bestTrackPoint = 0;
		speedHistory = null;


		loadSpline();
		speedHistory = new float[centerSpline.Length];
		speedHistoryIndex = 0;
		averageSpeed = 0;
		splitTimes = new TimeSpan[centerSpline.Length];
		splitVariance = null;
		lapTimer = 0;

		wrongDirectionCount = 0;
		pointHistory.Clear();

		lastLocation = transform.position;
	}

	[HideInInspector] public TimeSpan lapTime;
	[HideInInspector] public float topSpeed;
	[HideInInspector] public int bestTrackPoint;
	[HideInInspector] public float[] speedHistory = null;
	[HideInInspector] public int speedHistoryIndex;
	[HideInInspector] public double averageSpeed;
	[HideInInspector] public TimeSpan[] splitTimes;
	[HideInInspector] public float? splitVariance;
	[HideInInspector] public string failureReason;

	private float lapTimer = 0;
	private bool lapTimerRunning = false;

	public static int TrackSegments { get; private set; } = 500;


	ERRoad road;
	Vector3[] centerSpline;
	Vector3[] leftSpline;
	Vector3[] rightSpline;
	float roadWidth;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool IsRunning() {
		return hasStarted && !hasFailed && !hasFinished;
	}

	void Start() {
		var finishLineGO = GameObject.Find("FinishLine");
		if (finishLineGO == null) {
			throw new InvalidProgramException("Unable to locate an object in the scene called 'FinishLine'");
		}

		finishLine = finishLineGO.GetComponent<FinishLine>();
		if (finishLine == null) {
			throw new InvalidProgramException("Finish Line object doesn't contain a FinishLine script");
		}

		centerOfMass = this.transform.Find("CenterOfMass");
		if (centerOfMass == null) {
			throw new InvalidOperationException("Couln't locate vehicle Center of Mass");
		}
		DoReset();
	}

	private void loadSpline() {
		ERRoadNetwork network;
		try {
			network = new ERRoadNetwork();
		} catch (Exception e) {
			throw new ArgumentException("Couldn't load road network", e);
		}
		var roads = network.GetRoads();
		if (roads.Length == 0) {
			throw new ArgumentException("No roads!");
		}
		road = roads[0];
		centerSpline = road.GetSplinePointsCenter();
		leftSpline = road.GetSplinePointsLeftSide();
		rightSpline = road.GetSplinePointsRightSide();
		TrackSegments = centerSpline.Length;



		var points = road.GetMarkerPositions();
		if (!hasSquawked) {
			var rep = centerSpline.Select((x, i) => String.Format("{0}: {1}", i, x));
			hasSquawked = true;
			var msg = new StringBuilder();
			msg.AppendLine("Sensor suite online.");
			msg.AppendFormat("Road: {0}\n", road.gameObject.name);
			msg.AppendFormat("Width: {0}\n", road.GetWidth());
			msg.AppendFormat("Length: {0}\n", road.GetLength());
			msg.AppendFormat("Curve Markers: {0}\n", points.Length);
			msg.AppendFormat("Spline Points: {0:#,##0}/{1:#,##0}/{2:#,##0} [L//R]\n",
				leftSpline.Length,
				centerSpline.Length,
				rightSpline.Length);
			UnityEngine.Debug.Log(msg.ToString());
			UnityEngine.Debug.LogFormat("Spline:\n{0}\n...\n{1}",
				String.Join(Environment.NewLine, rep.Take(10)),
				String.Join(Environment.NewLine, rep.Reverse().Take(10).Reverse()));
		}
		roadWidth = road.GetWidth();
	}

	// Update is called once per frame

	private void FixedUpdate() {
		timeSinceProgressMade += Time.fixedDeltaTime;
		if (!hasFailed && timeSinceProgressMade > maxIdleTime) {
			HandleFailure("Stalled");
		}
		if (road != null) {
			UpdateSpeed();
			if (speedHistory.Length == 0) {
				speedHistory = new float[centerSpline.Length];
			}
			var spF = FindSmoothedSplineIndex();
			int sp = Mathf.RoundToInt(spF);

			if (sp >= 0) {
				if (referenceIndex != sp) {
					CheckNewLocation(sp);
				}
				referenceIndex = sp;
				UpdateTargets(spF);
			}
		} else {
			loadSpline();
		}
	}
	private void CheckNewLocation(int splinePoint) {
		pointHistory.Add(splinePoint);
		splinePoint = splinePoint % (centerSpline.Length - 1);
		timeSinceProgressMade = 0;

		var cst = finishLine.CurrentSplitTime();
		try {

			if (cst != null && cst.Length > 0 && cst[splinePoint] != TimeSpan.Zero) {
				splitVariance = (float)(lapTimer - cst[splinePoint].TotalSeconds);
			} else {
				splitVariance = null;
			}
		} catch (IndexOutOfRangeException) {
			UnityEngine.Debug.LogFormat("Attempting to access split time at index {0} of a spline with {1} items",
				splinePoint, cst == null ? "Null" : cst.Length.ToString());
			throw;
		}
		var correctDirection = false;
		if (bestTrackPoint == 0) {
			correctDirection = true;
		} else {
			// we compare to centerSpline.Length -2 rather than -1 as it contains a double-entry (first/last) for the loop. We'll never detect Length-1 as the same location in index 0 is preferred
			if ((splinePoint == 0 || splinePoint == 1) && referenceIndex >= centerSpline.Length - 2) {
				correctDirection = !FinishLine.ReverseDirection;
				//UnityEngine.Debug.LogFormat("Transition check 1: {0} [{1}-{2}]", correctDirection, referenceIndex, splinePoint);
			} else if ((referenceIndex == 0 || referenceIndex == 1) && splinePoint >= centerSpline.Length - 2) {
				correctDirection = FinishLine.ReverseDirection;
				//UnityEngine.Debug.LogFormat("Transition check 2: {0} [{1}-{2}]", correctDirection, referenceIndex, splinePoint);
			} else {
				correctDirection = FinishLine.ReverseDirection ? (splinePoint < referenceIndex) : (splinePoint > referenceIndex);
				//UnityEngine.Debug.LogFormat("Transition check 3: {0} [{1}-{2}]", correctDirection, referenceIndex, splinePoint);
			}
		}
		if (!correctDirection) {
			wrongDirectionCount++;
			bestTrackPoint -= 1;
			if (!hasFailed
				&& wrongDirectionCount > 1) {
				UnityEngine.Debug.LogFormat("Driver failed: Went the wrong way (history {0})", String.Join(", ", pointHistory));
				HandleFailure("Wrong Direction");
			}
		} else {
			wrongDirectionCount = 0;
			if (IsRunning() && speedHistoryIndex < speedHistory.Length) {
				speedHistory[speedHistoryIndex++] = speed;
				splitTimes[splinePoint] = GetLapTimeSpan();
				averageSpeed = ((averageSpeed * (speedHistoryIndex - 1)) + speed) / speedHistoryIndex;
				bestTrackPoint += 1;
			}
			if (finishLine.goal == splinePoint) {
				if (!hasStarted) {
					lapTimerRunning = true;
					hasStarted = true;
					hasFailed = false; // Just in case we OOB before reaching start line
				} else if (!hasFinished && !hasFailed && bestTrackPoint > centerSpline.Length / 2) {
					lapTimerRunning = false;
					hasFinished = true;
				}
			}
		}
	}

	private TimeSpan GetLapTimeSpan() {
		return TimeSpan.FromSeconds(lapTimer);
	}

	public void HandleFailure(string reason) {
		if (!hasFailed && !hasFinished) {
			failureReason = reason;
			hasFailed = true;
			lapTimerRunning = false;
			
		}
	}


	private void Update() {
		if (lapTimerRunning) {
			lapTimer += Time.deltaTime;
		}
	}

	private void UpdateSpeed() {
		centerDistance = (GetSplinePoint(ref centerSpline, referenceIndex) - this.transform.position).magnitude;
		variance = centerDistance / roadWidth / 1.5f;
		onTrack = centerDistance < roadWidth * .75f;

		var spd = (transform.position - lastLocation).magnitude / Time.fixedDeltaTime;
		var forward = Vector3.Angle(transform.forward, (lastLocation - transform.position).normalized) > 90;
		speed = spd * (forward ? 1 : -1);
		lastLocation = transform.position;

		if (hasStarted && !hasFinished) {
			//Only set these during an active run
			if (!hasFailed) {
				if (!onTrack) {
					//UnityEngine.Debug.LogFormat("Driver failed: Went off the track, distance {0}", centerDistance);
					HandleFailure("Off Track");
				}
				if (lapTime.TotalSeconds > maxTestDuration) {
					//UnityEngine.Debug.LogFormat("Driver failed: Max test time elasped");
					HandleFailure("Time Up");
				}
			}
			if (speed > topSpeed) { topSpeed = speed; }
		}

		var dst = (nearTarget - this.transform.position).magnitude;
		speedToGoal = (lastGoalDistance - dst) / Time.fixedDeltaTime;
		lastGoalDistance = dst;
		lapTime = GetLapTimeSpan();
	}


	void UpdateTargets(float i) {
		roadCenter = GetSmoothSplinePoint(ref centerSpline, i);
		nearTarget = GetSmoothSplinePoint(ref centerSpline, i + 5 * (FinishLine.ReverseDirection ? -1 : 1));
		midTarget = GetSmoothSplinePoint(ref centerSpline, i + 20 * (FinishLine.ReverseDirection ? -1 : 1));
		farTarget = GetSmoothSplinePoint(ref centerSpline, i + 50 * (FinishLine.ReverseDirection ? -1 : 1));

		//target1.transform.position = Vector2.SmoothDamp(target1.transform.position, nearTarget, ref t1Vel, .1f);
		//target2.transform.position = Vector2.SmoothDamp(target2.transform.position, farTarget, ref t2Vel, .1f);
	}






	private Vector3 GetSmoothSplinePoint(ref Vector3[] spline, float splineIndex) {
		var l = Mathf.FloorToInt(splineIndex);
		var p1 = GetSplinePoint(ref spline, l);
		var p2 = GetSplinePoint(ref spline, Mathf.CeilToInt(splineIndex));
		var prog = (splineIndex - l);
		return p1 * (1 - prog) + p2 * prog;
	}


	private Vector3 GetSplinePoint(ref Vector3[] spline, int splineIndex) {
		return spline[WrapIndex(spline.Length, splineIndex)];
	}

	private int WrapIndex(int length, int splineIndex) {
		var wrapped = ((splineIndex % length) + length) % length;
		if (wrapped == length - 1) {
			wrapped = 0;
		}
		return wrapped;
	}


	private float FindSmoothedSplineIndex() {
		int i = CheapFindClosestSplinePoint(centerOfMass.position);
		var p1 = GetSplinePoint(ref centerSpline, i - 1);
		var p2 = GetSplinePoint(ref centerSpline, i + 1);
		var fraction = ClosestPositionOnLine(p1, p2, transform.position);
		var ret = i + fraction * 2 - 1;
		return ret;
	}

	private float ClosestPositionOnLine(Vector3 start, Vector3 end, Vector3 point) {
		var line = (end - start);
		var len = line.magnitude;
		line.Normalize();

		var v = point - start;
		var d = Vector3.Dot(v, line);
		d = Mathf.Clamp(d, 0f, len);
		return d / len;
	}

	private int CheapFindClosestSplinePoint(Vector3 location) {
		if (referenceIndex < 0) {
			return ExpensiveClosestSplinePoint(location);
		} else {
			float distance = float.MaxValue;
			int ret = -1;
			for (int i = referenceIndex - 5; i <= referenceIndex + 5; i++) {
				var cmp = (location - GetSplinePoint(ref centerSpline, i)).magnitude;
				if (cmp < distance) {
					distance = cmp;
					ret = i;
				}
			}

			return WrapIndex(centerSpline.Length, ret);
		}
	}

	//private int CheapFindClosestSplinePoint(Vector3 location) {
	//	//Must be a multiple of 5
	//	int initialStepSize = 25;
	//	float distance = (location - centerSpline[centerSpline.Length - 1]).magnitude;
	//	int ret = centerSpline.Length - 2;

	//	for (int i = 0; i < centerSpline.Length; i += initialStepSize) {
	//		var cmp = (location - centerSpline[i]).magnitude;
	//		if (cmp < distance) {
	//			distance = cmp;
	//			ret = i;
	//		}
	//	}

	//	for (int i = ret - initialStepSize; i <= ret + initialStepSize; i += initialStepSize / 5) {

	//		var cmp = (location - GetSplinePoint(ref centerSpline, i)).magnitude;
	//		if (cmp < distance) {
	//			distance = cmp;
	//			ret = i;
	//		}
	//	}

	//	for (int i = ret - initialStepSize / 5; i <= ret + initialStepSize / 5; i++) {
	//		var cmp = (location - GetSplinePoint(ref centerSpline, i)).magnitude;
	//		if (cmp < distance) {
	//			distance = cmp;
	//			ret = i;
	//		}
	//	}

	//	ret = WrapIndex(centerSpline.Length, ret);
	//	return ret;

	//}
	private int ExpensiveClosestSplinePoint(Vector3 location) {
		float distance = float.MaxValue;
		int ret = -1;

		for (int i = 0; i < centerSpline.Length; i++) {
			var cmp = (location - centerSpline[i]).magnitude;
			if (cmp < distance) {
				distance = cmp;
				ret = i;
			}
		}
		return ret;
	}

}