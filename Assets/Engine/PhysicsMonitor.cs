﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsMonitor : MonoBehaviour {
	// Start is called before the first frame update

	private float goal;
	[HideInInspector] public float latest;
	void Start() {
		goal = Time.fixedDeltaTime;
	}

	// Update is called once per frame
	void FixedUpdate() {
		latest = Time.fixedDeltaTime;
		if (latest != goal) {
			Debug.LogFormat("Performance: Fixed update slipping {0}", latest);
		}
	}
}
