﻿using EasyRoads3Dv3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class SceneEditor : EditorWindow {
	// Start is called before the first frame update

	private int probeTrackDensity = 100;

	private GameObject scene;
	private float lowHeight = 0.1f;
	private float highHeight = 20f;
	private int intermediateLayers = 3;
	private bool coverTerrain = true;
	private bool includeCeiling = false;
	private Vector2Int terrainSlices = new Vector2Int(100, 100);

	void Start() {
	}

	// Update is called once per frame
	void Update() {
		if (scene == null) {
			scene = GameObject.Find("Scene");
		}
	}

	[MenuItem("Scene/Scene Helper")]
	private static void DoIt() { EditorWindow.GetWindow<SceneEditor>(); }

	private void OnGUI() {
		GUILayout.Label("Light Probes");

		GUILayout.BeginHorizontal();
		GUILayout.Label("Min/Max height above ground");
		lowHeight = EditorGUILayout.FloatField(lowHeight);
		highHeight = EditorGUILayout.FloatField(highHeight);
		GUILayout.EndHorizontal();

		intermediateLayers = EditorGUILayout.IntField("Intermediate slices", intermediateLayers);
		

		coverTerrain = EditorGUILayout.Toggle("Cover terrain", coverTerrain);
		includeCeiling = EditorGUILayout.Toggle("Include ceiling", includeCeiling);

		if (coverTerrain) {
			GUILayout.BeginHorizontal();
			GUILayout.Label("Terrain slices");
			terrainSlices = new Vector2Int(
				EditorGUILayout.IntField(terrainSlices.x),
				EditorGUILayout.IntField(terrainSlices.y));
			GUILayout.EndHorizontal();
		}
		probeTrackDensity = EditorGUILayout.IntField("Track segments", probeTrackDensity);

		EditorGUILayout.Separator();

		if (GUILayout.Button("Build Light Probes")) {
			BuildLightProbes();
		}

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Show Indicators")) {
			ShowLightIndicators();
		}

		if (GUILayout.Button("Destroy Indicators")) {
			DestroyLightIndicators();
		}
		GUILayout.EndHorizontal();
	}

	private void DestroyLightIndicators() {
		var go = GetContainer("Lighting Indicators");
		for (int i = go.transform.childCount - 1; i >= 0; i--) {
			GameObject.DestroyImmediate(go.transform.GetChild(i).gameObject);
		}
		GameObject.DestroyImmediate(go);
	}


	private void ShowLightIndicators() {
		DestroyLightIndicators();
		//var points = new List<Vector3>();
		var go = GetContainer("Lighting Indicators");
		//var road = GetRoad();
		//var leftSpline = road.GetSplinePointsLeftSide();
		//var centerSpline = road.GetSplinePointsCenter();
		//var rightSpline = road.GetSplinePointsRightSide();
		//for (int i = 0; i < centerSpline.Length; i += 1) {
		//	points.Add(leftSpline[i]);
		//	points.Add(centerSpline[i]);
		//	points.Add(centerSpline[i] + Vector3.up);
		//	points.Add(rightSpline[i]);
		//	points.Add(leftSpline[i] + Vector3.up * 2);
		//	points.Add(centerSpline[i] + Vector3.up * 2);
		//	points.Add(rightSpline[i] + Vector3.up * 2);
		//}


		var points = GetLightProbeGroup().probePositions;

		foreach (var p in points) {
			var s = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			GameObject.DestroyImmediate(s.GetComponent<SphereCollider>());
			s.transform.parent = go.transform;
			s.transform.position = p;
			var r = s.GetComponent<MeshRenderer>();
			r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			r.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.BlendProbes;
			s.hideFlags = HideFlags.HideAndDontSave;
		}
	}

	private GameObject GetContainer(string name) {
		var li = scene.transform.Find(name);
		GameObject liObject;
		if (li != null) {
			liObject = li.gameObject;
		} else {
			liObject = new GameObject(name);
			liObject.transform.parent = scene.transform;
		}
		return liObject;
	}


	private ERRoad GetRoad() {
		ERRoadNetwork network;
		try {
			network = new ERRoadNetwork();
		} catch (Exception e) {
			throw new ArgumentException("Couldn't load road network", e);
		}
		var roads = network.GetRoads();
		if (roads.Length == 0) {
			throw new ArgumentException("No roads!");
		}
		return roads[0];
	}

	private Terrain GetTerrain() {
		return GameObject.Find("Terrain 0,0").GetComponent<Terrain>();
	}


	private void BuildLightProbes() {
		GetLightProbeGroup().probePositions = CalculateLightProbePositions();
	}

	private LightProbeGroup GetLightProbeGroup() {
		var lpO = GetContainer("LightProbes");
		LightProbeGroup lightProbeGroup = lpO.GetComponent<LightProbeGroup>();
		if (lightProbeGroup == null) {
			lightProbeGroup = lpO.AddComponent<LightProbeGroup>();
		}
		return lightProbeGroup;
	}


	private Vector3[] CalculateLightProbePositions() {
		var heights = new List<float>();
		for (int h = 0; h <= intermediateLayers + 1; h++) {
			heights.Add(1f * h / (intermediateLayers + 1) * (highHeight - lowHeight) + lowHeight);
		}
		Debug.LogFormat("Light probe heights: {0}", String.Join(", ", heights));
		var terrain = GetTerrain();
		if (scene == null) {
			throw new Exception("No object named 'Scene' found");
		}
		if (terrain == null) {
			throw new Exception("No primary terrain found");
		}
		var road = GetRoad();
		var leftSpline = road.GetSplinePointsLeftSide();
		var centerSpline = road.GetSplinePointsCenter();
		var rightSpline = road.GetSplinePointsRightSide();


		List<Vector3> rawPositions = new List<Vector3>();


		// Terrain
		if (coverTerrain) {
			var terPos = terrain.gameObject.transform.position;
			var terScale = terrain.terrainData.size;
			for (int sliceX = 0; sliceX <= terrainSlices.x; sliceX++) {
				for (int sliceY = 0; sliceY <= terrainSlices.y; sliceY++) {
					var relX = 1f * sliceX / terrainSlices.x;
					var relY = 1f * sliceY / terrainSlices.y;
					var height = terrain.terrainData.GetInterpolatedHeight(relX, relY);



					var pos = Vector3.Scale(
							terScale,
							new Vector3(relX, 0, relY))
						+ height * Vector3.up
						+ terPos;

					//Debug.LogFormat("{0},{1}: {2:0.00}, {3:0.00} @ {4} = {5}", sliceX, sliceY, relX, relY, height, pos);

					rawPositions.Add(pos);

				}
			}
		}






		// Track
		for (float f = 0; f < probeTrackDensity; f++) {
			var splinePoint = (int)(f / probeTrackDensity * leftSpline.Length);
			var perpVec = (rightSpline[splinePoint] - leftSpline[splinePoint]).normalized*2;
			//var perp = perpVec.normalized;
				rawPositions.AddRange(new Vector3[] {
					leftSpline[splinePoint] - perpVec,
					leftSpline[splinePoint],
					centerSpline[splinePoint],
					rightSpline[splinePoint],
					rightSpline[splinePoint] + perpVec,
				});
		}




		var positions = heights
			.SelectMany(h => rawPositions.Select(r => r + Vector3.up * h)).ToList();

		if (includeCeiling) { 
			positions.AddRange(rawPositions.Select(x => new Vector3(x.x, 100, x.z)));
		}



		return positions.ToArray();

	}
}
