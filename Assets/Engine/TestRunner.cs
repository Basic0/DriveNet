﻿using DriveNet.Assets.Engine.Core;
using DriveNet.Assets.Engine.Core.NeuralNetwork;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TestRunner : MonoBehaviour {
	public static int MaxConcurrentTests = 50;
	public static bool QuickSpawn;

	UIManager ui;
	public AutoResetEvent carSpawned = new AutoResetEvent(false);
	public ReportManager reportManager;
	public UnityEvent NewTestCase;
	private const float CleanUpDelay = 10;
	private float timeSinceLastSpawn = 1000;

	public TimeSpan RunTime { get => sw.Elapsed; }
	private Stopwatch sw = new Stopwatch();

	public GameObject deathPlaceholder;
	[HideInInspector] public FinishLine finishLine;

	public bool IsHalted { get; set; }


	private List<TestCase> tests = new List<TestCase>();

	private ConcurrentStack<TestCase> driverQueue = new ConcurrentStack<TestCase>();
	public int GetDriverQueueLength() { return driverQueue.Count; }

	CarPool carPool;

	public List<TestCase> ActiveTests() {
		return tests.ToList();
	}

	void Start() {
		IsHalted = Application.isEditor;
		Initialise();
		ui = GameObject.Find("UI").GetComponent<UIManager>();
		carPool = GameObject.Find("CarPool").GetComponent<CarPool>();
	}



	private System.Random rnd = new System.Random();
	public void AddCandidate(IDriver driver, bool asGhost = false) {
		driverQueue.Push(new TestCase {
			Driver = driver,
			IsGhost = asGhost,
			Colour = new Color(
				 (float)(rnd.NextDouble() * .5 + .5),
				 (float)(rnd.NextDouble() * .5 + .5),
				 (float)(rnd.NextDouble() * .5 + .5))
		});
	}

	private void Initialise() {
		reportManager = new ReportManager();
		reportManager.StartRun();
		var finishLineGO = GameObject.Find("FinishLine");
		if (finishLineGO == null) {
			throw new InvalidProgramException("Unable to locate an object in the scene called 'FinishLine'");
		}

		finishLine = finishLineGO.GetComponent<FinishLine>();
		if (finishLine == null) {
			throw new InvalidProgramException("Finish Line object doesn't contain a FinishLine script");
		}

	}

	private void TweakRunSize(bool increase) {
		var ret = MaxConcurrentTests / 5;
		if (ret < 10) {
			ret += increase ? 1 : -1;
		} else {
			ret += increase ? 2 : -2;
		}

		ret = Mathf.Clamp(ret, 0, 200);
		ret = ret * 5;
		if (ret == 0) {
			ret = 1;
		}
		MaxConcurrentTests = ret;
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.KeypadMultiply)) {
			QuickSpawn = !QuickSpawn;
		}

		if (Input.GetKeyDown(KeyCode.LeftBracket)) {
			TweakRunSize(false);
		}
		if (Input.GetKeyDown(KeyCode.RightBracket)) {
			TweakRunSize(true);
		}

		if (Input.GetKeyDown(KeyCode.I)) {
			DoSpawn();
		}

		if (Input.GetKeyDown(KeyCode.H)) {
			IsHalted = !IsHalted;
		}



		timeSinceLastSpawn += Time.deltaTime;
		var cleanup = tests.Where(x => !x.IsActive()).ToList();
		// UnityEngine.Debug.LogFormat("Removing {0} test cases from {1}", cleanup.Count, tests.Count);
		foreach (var t in cleanup) {
			tests.Remove(t);
			StartCoroutine(DelayedKill(t));
		}
		while (!IsHalted
			&& !driverQueue.IsEmpty
			&& tests.Count < MaxConcurrentTests
			&& (QuickSpawn
			|| timeSinceLastSpawn > .25)) {
			if (!sw.IsRunning) {
				sw.Reset();
				sw.Start();
			}
			DoSpawn();
		}
		if (tests.Count == 0) {
			sw.Stop();
		}
	}

	private IEnumerator<YieldInstruction> DelayedKill(TestCase t) {
		if (t.Sensors.hasFinished) {
			finishLine.fireworks.Celebrate(new Color(
				(t.Colour.r - .5f) * 2,
				(t.Colour.g - .5f) * 2,
				(t.Colour.b - .5f) * 2));
		}
		var wheels = t.Vehicle.transform.Find("Wheels").GetComponentsInChildren<WheelCollider>();
		var report = t.Vehicle.GetComponent<TestDummy>().BuildReport();
		if (!t.IsGhost) {
			if (report == null) {
				throw new InvalidProgramException("Failed to build a report! Something's gone horribly wrong");
			}
			report.Config = t.Driver.Config;
			reportManager.SaveReport(report);
		}
		yield return new WaitForSeconds(1);
		if (UIManager.GetCurrentVehicle() == t.Vehicle) {
			yield return new WaitForSeconds(1);
			if (UIManager.GetCurrentVehicle() == t.Vehicle) {
				if (t.Sensors.hasFailed) {
					ui.ViewBestScore();
				} else {
					ui.ViewLatest();
				}
			}
			yield return new WaitForSeconds(CleanUpDelay - 1);
		} else {
			yield return new WaitForSeconds(CleanUpDelay);
		}
		carPool.ReturnVehicle(t.Vehicle);
	}

	public void DoSpawn() {
		if (driverQueue.Count > 0) {
			timeSinceLastSpawn = 0;
			GameObject vehicle = carPool.GetVehicle(finishLine.spawnPoint, finishLine.CurrentSpawnOrientation());
			var dummy = vehicle.GetComponent<TestDummy>();
			//var rb = vehicle.GetComponent<Rigidbody>();
			//rb.velocity = Vector3.zero;
			TestCase testCase;
			if (driverQueue.TryPop(out testCase)) {
				dummy.IsGhost = testCase.IsGhost;
				testCase.Vehicle = vehicle;
				testCase.Sensors = vehicle.GetComponent<SensorSuite>();
				testCase.TestDummy = dummy;
			}
			dummy.SetDriver(testCase.Driver, testCase.Colour);
			dummy.SetDriverActive(true);

			//Debug.Log("Notifying of new test case");
			tests.Add(testCase);
			//UnityEngine.Debug.LogFormat("Adding test case, now {0}", tests.Count);
			NewTestCase.Invoke();
		}
	}
}
