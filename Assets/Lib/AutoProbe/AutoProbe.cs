﻿//-------------------
// Copyright 2019
// Reachable Games, LLC
//-------------------

using UnityEngine;
#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine.Rendering;
using UnityEditor;
#endif

namespace ReachableGames
{
	namespace AutoProbe
	{
		[ExecuteInEditMode]
		public class AutoProbe : MonoBehaviour 
		{
#if UNITY_EDITOR
			public enum GeneratorType
			{
				Grid,
				Spray,
			}

			public GeneratorType generatorType = GeneratorType.Spray;
			public int rayCount = 10;
			public float maxDistance = 4.0f;
			public float minCollisionDistance = 3.0f;
			public float errorTolerance = 0.05f;
			public LayerMask layerMask = ~0;
		
			public List<GameObject> meshConstraints = new List<GameObject>();  // this could be any kind of collider / mesh / terrain / etc
			public float maxHeightAboveMeshes = 3.0f;

			private HashSet<MeshFilter> meshFilters = new HashSet<MeshFilter>();  // spawnObjects expands to these lists when you generate probes
			private HashSet<Terrain> terrains = new HashSet<Terrain>();
			private HashSet<Collider> meshColliders = new HashSet<Collider>();
			private float geometryBackoff = 0.01f;  // when a ray hits collision geometry, we step back this much so the light probe knows what side it's on

			public void Awake()
			{
				UpdateDisabledChildColliders(false);
				if (disabledChildColliders.Length==0)  // initialize a child collider set so the user isn't confused how to set it up
				{
					GameObject constraints = new GameObject("Constraints");
					GameObject box = new GameObject("Box");
					GameObject sphere = new GameObject("Sphere");
					GameObject capsule = new GameObject("Capsule");
					constraints.transform.SetParent(gameObject.transform, false);
					box.transform.SetParent(constraints.transform, false);
					sphere.transform.SetParent(constraints.transform, false);
					capsule.transform.SetParent(constraints.transform, false);
					box.AddComponent<BoxCollider>().enabled = false;
					sphere.AddComponent<SphereCollider>().enabled = false;
					capsule.AddComponent<CapsuleCollider>().enabled = false;
				}
			}

			// Returns the count of new probes generated
			public int GenerateProbes()
			{
				try
				{
					// Figure out where we can create probes.
					if (UpdateDisabledChildColliders(true))
						return 0;  // aborted

					Vector3[] probePositions = new Vector3[0];
					{
						LightProbeGroup lpg = GetComponent<LightProbeGroup>();
						if (lpg!=null)
						{
							probePositions = lpg.probePositions;
							Undo.DestroyObjectImmediate(lpg);
						}
					}

					// expand spawnObjects lists to include meshes, terrain, and colliders
					meshFilters.Clear();
					terrains.Clear();
					meshColliders.Clear();
					foreach (GameObject g in meshConstraints)
					{
						MeshFilter[] mfs = g.GetComponentsInChildren<MeshFilter>();
						Terrain[] ts = g.GetComponentsInChildren<Terrain>();
						Collider[] cs = g.GetComponentsInChildren<Collider>();
						foreach (MeshFilter mf in mfs) 
							meshFilters.Add(mf);
						foreach (Terrain t in ts) 
							terrains.Add(t);
						foreach (Collider c in cs) 
							meshColliders.Add(c);
					}

					InitSpatialHash(maxDistance);  // resolution MUST be at least half the largest query size

					// Grab all the light probe positions and move them from local space to world space
					List<Vector3> p = new List<Vector3>();
					p.Capacity = probePositions.Length;
					for (int i=0; i<probePositions.Length; i++)
					{
						Vector3 wsPos = transform.TransformPoint(probePositions[i]);  // move to world space
						p.Add(wsPos);
						AddToSpatialHash(wsPos);
						if (i % 100 == 0)
							EditorUtility.DisplayProgressBar("AutoProbe: Generating Light Probes ("+gameObject.name+")", "Transforming Points", i / (float)p.Count);
					}
					if (p.Count==0)  // seed this in case all probe positions are empty
					{
						if (IsInsideBounds(transform.position))
						{
							p.Add(transform.position);
							AddToSpatialHash(transform.position);  // use disabled collider locations and the autoprobe object itself
						}

						// initialize with the center points of all the disabled colliders too
						foreach (Collider c in disabledChildColliders)
						{
							p.Add(c.bounds.center);
							AddToSpatialHash(c.bounds.center);
						}

						foreach (MeshFilter mf in meshFilters)  // if meshes are selected, spawn at their vertex positions and above them.
						{
							Vector3[] verts = mf.sharedMesh.vertices;
							foreach (Vector3 v in verts)
							{
								Vector3 pos = mf.transform.TransformPoint(v);  // put v into world space
								pos.y += geometryBackoff;
								if (IsInsideBounds(pos))
								{
									p.Add(pos);
									AddToSpatialHash(pos);
								}

								pos.y += maxHeightAboveMeshes;
								if (IsInsideBounds(pos))
								{
									p.Add(pos);
									AddToSpatialHash(pos);
								}
							}
						}
						foreach (Terrain t in terrains)  // if terrains are selected, spawn at their vertex positions and above them.
						{
							for (int x=0; x<100; x++)
							{
								for (int z=0; z<100; z++)
								{
									Vector3 wsPos = t.GetPosition() + new Vector3(x/100.0f*t.terrainData.heightmapScale.x*t.terrainData.heightmapResolution, 0, z/100.0f*t.terrainData.heightmapScale.z*t.terrainData.heightmapResolution);
									wsPos.y += t.SampleHeight(wsPos) + geometryBackoff;

									if (IsInsideBounds(wsPos))
									{
										p.Add(wsPos);
										AddToSpatialHash(wsPos);
									}

									wsPos.y += maxHeightAboveMeshes;
									if (IsInsideBounds(wsPos))
									{
										p.Add(wsPos);
										AddToSpatialHash(wsPos);
									}
								}
							}
						}
					}

					// Now, create an "active" set which we can work with, since a lot of probes will not be on the advancing surface.
					Queue<Vector3> active = new Queue<Vector3>(p);
					int progressTotal = p.Count;
					int progress = 0;
					while (active.Count>0)
					{
						if (EditorUtility.DisplayCancelableProgressBar("AutoProbe: Generating Light Probes ("+gameObject.name+")", "Raymarching... Active ["+active.Count+"]  Total ["+p.Count+"]", progress / (float)progressTotal))
							break;

						Vector3 currentPoint = active.Dequeue();
						progress++;

						switch (generatorType)
						{
							case GeneratorType.Grid:  // cast rays in 6 directions.  MinCollisionDistance is not meaningfully tunable in Grid mode, so just set it small so it gets close to geometry.
							{
								if (DoCast(active, p, currentPoint, Vector3.forward, 0.01f, maxDistance)) progressTotal++;
								if (DoCast(active, p, currentPoint, Vector3.back, 0.01f, maxDistance)) progressTotal++;
								if (DoCast(active, p, currentPoint, Vector3.right, 0.01f, maxDistance)) progressTotal++;
								if (DoCast(active, p, currentPoint, Vector3.left, 0.01f, maxDistance)) progressTotal++;
								if (DoCast(active, p, currentPoint, Vector3.up, 0.01f, maxDistance)) progressTotal++;
								if (DoCast(active, p, currentPoint, Vector3.down, 0.01f, maxDistance)) progressTotal++;
								break;
							}
							case GeneratorType.Spray:  // randomly cast N rays
							{
								bool keepActive = false;
								for (int i=0; i<rayCount; i++)
								{
									if (DoCast(active, p, currentPoint, Random.onUnitSphere, minCollisionDistance, maxDistance))
									{
										keepActive = true;
										progressTotal++;
									}
								}
								if (keepActive)  // in the case where a point generated a new adjacent point, there may yet be unexplored space nearby still, due to sampling error.  Keep trying.
								{
									active.Enqueue(currentPoint);
									progressTotal++;
								}
								break;
							}
						}
					}

					// Move all world space points back to local space and assign to light probe group
					for (int i=0; i<p.Count; i++)
					{
						p[i] = transform.InverseTransformPoint(p[i]);
						if (i % 100 == 0)
							EditorUtility.DisplayProgressBar("AutoProbe: Generating Light Probes ("+gameObject.name+")", "Transforming Points", i / (float)p.Count);
					}

					// force the update of the inspector
					{
						LightProbeGroup lpg = Undo.AddComponent<LightProbeGroup>(gameObject);
						Undo.RecordObject(lpg, "Generate Light Probes");
						lpg.probePositions = p.ToArray();
					}
					Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
					Undo.SetCurrentGroupName("Generate Light Probes");
					int newProbes = p.Count - probePositions.Length;
					return newProbes;
				}
				catch (System.Exception e)
				{
					Debug.LogException(e);
				}
				finally
				{
					EditorUtility.ClearProgressBar();
				}
				return 0;
			}

			static private RaycastHit[] oneHit = new RaycastHit[1];
			static private RaycastHit[] manyHits = new RaycastHit[100];

			// Cast a ray in some direction and if it hits no geometry, check for nearby probes already, and if there aren't any, make one.  
			// If it hits something, back off slightly, and if its length is at least minCollisionDistance, make a probe there.  Otherwise fail.  
			// Return true if we made a probe.
			private bool DoCast(Queue<Vector3> activeList, List<Vector3> allPoints, Vector3 pos, Vector3 dir, float minCollisionDist, float maxDistBetweenPoints)
			{
				Vector3 newPos = pos + dir * maxDistBetweenPoints;
		
				// reject any point outside the bounding radius for this node (which also checks for the height above mesh, if AboveMeshes is enabled)
				if (IsInsideBounds(newPos)==false)
					return false;

				float distBetweenPoints = maxDistBetweenPoints * 0.99f;  // reduce slightly to allow for floating point error
				int numhits = Physics.RaycastNonAlloc(pos, dir, oneHit, distBetweenPoints, layerMask, QueryTriggerInteraction.UseGlobal);
				if (numhits>0)
				{
					newPos = oneHit[0].point - dir * geometryBackoff;
					if (oneHit[0].distance - geometryBackoff < minCollisionDist)
						return false;  // this point is too close
					distBetweenPoints = minCollisionDist;  // reduce the min tolerance so we don't end up with an unlimited number of surface points
				}

				// Let's see if there's any point closer than distBetweenPoints, WHICH WE CAN RAYCAST TO.  If so, reject it.  If not, add it.
				if (HasNearbyPoint(newPos, distBetweenPoints))
					return false;
			
				activeList.Enqueue(newPos);
				allPoints.Add(newPos);
				AddToSpatialHash(newPos);
				return true;
			}

			// returns the number of probes removed
			public int OptimizeProbes()
			{
				try
				{
					int totalRemoved = 0;
					Vector3[] probePositions = new Vector3[0];
					{
						LightProbeGroup lpg = GetComponent<LightProbeGroup>();
						if (lpg!=null)
						{
							probePositions = lpg.probePositions;  // Grab all the light probe positions
							Undo.DestroyObjectImmediate(lpg);
						}
					}

					// move them from local space to world space
					EditorUtility.DisplayProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Moving to world space", 0.0f);
					int initialProbes = probePositions.Length;
					List<Vector3> p = new List<Vector3>();
					p.Capacity = initialProbes;
					for (int i = 0; i < initialProbes; i++)
					{
						Vector3 wsPos = transform.TransformPoint(probePositions[i]);
						p.Add(wsPos);
						probePositions[i] = wsPos;
						if (i % 100 == 0)
							EditorUtility.DisplayProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Moving to world space", i/(float)initialProbes);
					}
					EditorUtility.DisplayProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Moving to world space", 1.0f);

					while (p.Count >= 5)  // too few points means skip optimization.  Nothing to remove.
					{
						// Tetrahedralize the whole set of lightprobes, removing the junk points first.
						int[] tetraIndices;
						Vector3[] positions;
						EditorUtility.DisplayCancelableProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Generating tetrahedrons... Probes [" + initialProbes + "]  Removed [" + totalRemoved + "]", totalRemoved / (float)initialProbes);
						Lightmapping.Tetrahedralize(probePositions, out tetraIndices, out positions);
						if (positions.Length != p.Count)  // copy back the proper positions to be used
						{
							p.RemoveRange(positions.Length, p.Count - positions.Length);
							for (int i = 0; i < positions.Length; i++)
							{
								p[i] = positions[i];
							}
						}
						if (p.Count < 5)  // skip optimization.  Nothing to remove.
						{
							break;
						}

						// Create adjacency neighborhoods for each point, so I can check each one and see if they are necessary.  This is a pretty memory intensive data structure, but temporary.
						// We do this by walking the tetrahedrons and adding all the vertices in the tetrahedron to each of the vertices IN that tetrahedron.  Then we sort/unique each list, so it has no redundancies.
						List<HashSet<int>> adjacencyVerts = new List<HashSet<int>>(positions.Length);
						SphericalHarmonicsL2[] originalSH = new SphericalHarmonicsL2[positions.Length];
						SphericalHarmonicsL2 interpProbe = new SphericalHarmonicsL2();
						SphericalHarmonicsL2[] corners = new SphericalHarmonicsL2[4];
						SphericalHarmonicsL2 tempProbe = new SphericalHarmonicsL2();
						for (int i=0; i<positions.Length; i++)
						{
							LightProbes.GetInterpolatedProbe(positions[i], null, out tempProbe);
							originalSH[i] = tempProbe;  // cache all the original SH before we jack with them.
							adjacencyVerts.Add(new HashSet<int>());  // make space for all the verts-to-tetras lists.
						}
						for (int i = 0; i < tetraIndices.Length; i+=4)  // step by the tetrahedron
						{
							for (int j=0; j<4; j++)  // for each vertex in the tetrahedron
							{
								for (int k = 0; k < 4; k++)  // add all the vertices in this tetrahedron TO EACH VERTEX'S LIST.
								{
									adjacencyVerts[tetraIndices[i+j]].Add(tetraIndices[i+k]);
								}
							}
						}

						// Walk each vertex and regenerate tetrahedrons for its list of adjacencies.  Then regenerate again with that vertex absent from the list.  If the interpolation is within tolerance,
						// it is a redundant point and can be removed.  Since it's very, very complicated to remove multiple points from an area and figure out adjacencies again, it's better to just do that
						// in passes and keep doing passes until there is nothing more to remove.  Should be fairly quick anyway.
						int progress = 0;
						int totalProgress = positions.Length;
						HashSet<int> locked = new HashSet<int>();    // these are probes we will not attempt to optimize out on this pass, because one of his neighbors was optimized out already
						HashSet<int> toRemove = new HashSet<int>();  // these are probes we will optimize out
						for (int i=0; i<positions.Length; i++)
						{
							if ((i % 10 == 0) && EditorUtility.DisplayCancelableProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Interpolating baked light probes... Probes [" + initialProbes + "]  Removed [" + totalRemoved + "]", totalRemoved / (float)initialProbes))
								break;
							progress++;

							int numAdjVerts = adjacencyVerts[i].Count;
							if (numAdjVerts>4)  // can never remove a valence vertex from a tetrahedron.  There's no adjacency who can fill in for it
							{
								// Skip optimizing this tetrahedron if any of my adjacencies are locked.
								if (locked.Contains(i)==false)  // only attempt optimizing this vertex away if this specific vertex is not yet locked by an adjacent optimization
								{
									// Since we already cached all the light probe SH's, we just need to try making new tetras that don't include p[i] so we can interpolate it from corners.
									Vector3[] testPoints = new Vector3[numAdjVerts - 1];
									int[] originalIndices = new int[numAdjVerts - 1];
									int testIndex = 0;
									foreach (int vi in adjacencyVerts[i])
									{
										if (vi!=i)
										{
											testPoints[testIndex] = p[vi];  // make a list of positions that does not include p[i]
											originalIndices[testIndex] = vi;   // remember what the vertices were in this buffer, for reference
											testIndex++;
										}
									}

									// compute new tetrahedrons from a small set of points, not including the one we're testing						
									int[] tetraIndices2;
									Vector3[] positions2;
									Lightmapping.Tetrahedralize(testPoints, out tetraIndices2, out positions2);

									// Now, find the tetrahedron that contains the missing vertex position, using 3D barycentric coordinates.
									Vector3 pos = p[i];
									int bestTetraIndex = 0;
									float bestMax = Mathf.Infinity;
									float bestMin = Mathf.NegativeInfinity;
									Vector4 coordinates = Vector4.zero;
									for (int j = 0; j < tetraIndices2.Length; j += 4)
									{
										Vector3 a = positions2[tetraIndices2[j + 0]];
										Vector3 b = positions2[tetraIndices2[j + 1]];
										Vector3 c = positions2[tetraIndices2[j + 2]];
										Vector3 d = positions2[tetraIndices2[j + 3]];
										if (IsInsideTetrahedron(a, b, c, d, pos, ref coordinates))  // we found the tetra that holds our test point
										{
											bestTetraIndex = j;
											break;
										}
										else
										{
											// if this is better than the best coordinate we have seen, remember it
											float maxV = Mathf.Max(Mathf.Max(coordinates[0], coordinates[1]), Mathf.Max(coordinates[2], coordinates[3]));
											float minV = Mathf.Min(Mathf.Min(coordinates[0], coordinates[1]), Mathf.Min(coordinates[2], coordinates[3]));
											if (maxV <= bestMax && minV >= bestMin)
											{
												bestMax = maxV;
												bestMin = minV;
												bestTetraIndex = j;
											}
										}
									}

									// Make a decision about the interpolation, based on the best tetrahedron we found.  Usually it's inside, sometimes not perfectly.
	#if false
									LightProbes.GetInterpolatedProbe(a, null, out corners[0]);  // collect the tetrahedron corners and interpolate
									LightProbes.GetInterpolatedProbe(b, null, out corners[1]);
									LightProbes.GetInterpolatedProbe(c, null, out corners[2]);
									LightProbes.GetInterpolatedProbe(d, null, out corners[3]);

									// This is just checking to make sure my assumptions hold.
									if (CompareSH(corners[0], originalSH[originalIndices[tetraIndices2[bestTetraIndex + 0]]]) > errorTolerance)
										Debug.Log("Corner0 doesn't match its original SH.  Something funny about Unity's data handling of light probe data.");
									if (CompareSH(corners[1], originalSH[originalIndices[tetraIndices2[bestTetraIndex + 1]]]) > errorTolerance)
										Debug.Log("Corner1 doesn't match its original SH.  Something funny about Unity's data handling of light probe data.");
									if (CompareSH(corners[2], originalSH[originalIndices[tetraIndices2[bestTetraIndex + 2]]]) > errorTolerance)
										Debug.Log("Corner2 doesn't match its original SH.  Something funny about Unity's data handling of light probe data.");
									if (CompareSH(corners[3], originalSH[originalIndices[tetraIndices2[bestTetraIndex + 3]]]) > errorTolerance)
										Debug.Log("Corner3 doesn't match its original SH.  Something funny about Unity's data handling of light probe data.");
	#else
									// without having to re-compute, just pull these from the array we fetched initially
									corners[0] = originalSH[originalIndices[tetraIndices2[bestTetraIndex + 0]]];
									corners[1] = originalSH[originalIndices[tetraIndices2[bestTetraIndex + 1]]];
									corners[2] = originalSH[originalIndices[tetraIndices2[bestTetraIndex + 2]]];
									corners[3] = originalSH[originalIndices[tetraIndices2[bestTetraIndex + 3]]];
	#endif
									// Manually interpolating the Spherical Harmonic, we generate a new one and compare with what was baked.
									interpProbe = corners[0] * coordinates[0] + corners[1] * coordinates[1] + corners[2] * coordinates[2] + corners[3] * coordinates[3];

									float error = CompareSH(interpProbe, originalSH[i]);
									if (error < errorTolerance)
									{
	//									Debug.Log("Error tolerance is reasonable for probe " + i + " Err: " + error);
										// Note, if the SH we had originally is almost the same as the one we can generate using corner points and interpolation, let's throw it out.
										// lock all the verts
										toRemove.Add(i);
										foreach (int vIndex in originalIndices)  // originalIndices already excludes the point we are removing (i), so we just add the whole array to the lock set for this pass
										{
											locked.Add(vIndex);  // lock everything related to this set of tetrahedrons.  None of them can be removed.
										}
										totalRemoved++;
									}
								}
							}
						}

						// Recopy all the remaining probe points back to probePositions array, but keep them in WS
						probePositions = new Vector3[p.Count - toRemove.Count];
						int positionIndex = 0;
						for (int i = 0; i < p.Count; i++)
						{
							if (toRemove.Contains(i) == false)
							{
								probePositions[positionIndex] = p[i];
								positionIndex++;
							}
						}

						// keep optimizing until we stop removing points.
						if (toRemove.Count==0)
							break;
					}

					// Move all world space points back to local space and assign to light probe group.
					EditorUtility.DisplayProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Moving to object space", 0.0f);
					for (int i = 0; i < probePositions.Length; i++)
					{
						probePositions[i] = transform.InverseTransformPoint(probePositions[i]);
						if (i % 100 == 0)
							EditorUtility.DisplayProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Moving to world space", i/(float)probePositions.Length);
					}
					EditorUtility.DisplayProgressBar("AutoProbe: Optimizing Light Probes ("+gameObject.name+")", "Moving to world space", 1.0f);

					// force the update of the inspector
					{
						LightProbeGroup lpg = Undo.AddComponent<LightProbeGroup>(gameObject);
						Undo.RecordObject(lpg, "Optimize Probes");
						lpg.probePositions = probePositions;
					}
					Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
					Undo.SetCurrentGroupName("Optimize Probes");
					return totalRemoved;
				}
				catch (System.Exception e)
				{
					Debug.LogException(e);
				}
				finally
				{
					EditorUtility.ClearProgressBar();
				}
				return 0;
			}

			// NOTE: This is not numerically stable.  It is possible to have a point that is supposed to test inside but has a slightly negative (or perhaps slightly greater than 1.0) value.
			// The way to tell is if 3/4 coordinates are within the range, but one is just barely outside.
			static private bool IsInsideTetrahedron(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Vector3 test, ref Vector4 coordinates)
			{
				Vector3 vap = test - a;
				Vector3 vbp = test - b;

				Vector3 vab = b - a;
				Vector3 vac = c - a;
				Vector3 vad = d - a;

				Vector3 vbc = c - b;
				Vector3 vbd = d - b;
		
				float v6 = 1.0f / Vector3.Dot(vab, Vector3.Cross(vac, vad));
				coordinates[0] = Vector3.Dot(vbp, Vector3.Cross(vbd, vbc)) * v6;
				coordinates[1] = Vector3.Dot(vap, Vector3.Cross(vac, vad)) * v6;
				coordinates[2] = Vector3.Dot(vap, Vector3.Cross(vad, vab)) * v6;
				coordinates[3] = Vector3.Dot(vap, Vector3.Cross(vab, vac)) * v6;
				return !(coordinates[0] < 0.0f || coordinates[1] < 0.0f || coordinates[2] < 0.0f || coordinates[3] < 0.0f);  // any negatives means coordinate is OUTSIDE, so not of that means inside.
			}

			// This is all 6 major axis directions, plus 8 corner directions.  That should be fairly representative.
			static private Vector3[] directions = new Vector3[] 
				{ Vector3.forward, Vector3.back, Vector3.up, Vector3.down, Vector3.right, Vector3.left, 
					(new Vector3(1,1,1)).normalized, (new Vector3(-1,1,1)).normalized, (new Vector3(1,-1,1)).normalized, (new Vector3(1,1,-1)).normalized, 
					(new Vector3(-1,-1,1)).normalized, (new Vector3(-1,1,-1)).normalized,(new Vector3(1,-1,-1)).normalized, (new Vector3(-1,-1,-1)).normalized };
			static private Color[] aColors = new Color[directions.Length];
			static private Color[] bColors = new Color[directions.Length];
			static public float CompareSH(SphericalHarmonicsL2 a, SphericalHarmonicsL2 b)
			{
				float error = 0.0f;  // return the summed error
				a.Evaluate(directions, aColors);
				b.Evaluate(directions, bColors);
				for (int i=0; i<directions.Length; i++)
				{
					error += Mathf.Abs(aColors[i].r - bColors[i].r);
					error += Mathf.Abs(aColors[i].g - bColors[i].g);
					error += Mathf.Abs(aColors[i].b - bColors[i].b);
				}
				return error;
			}

			private Collider[] disabledChildColliders = null;
			public bool UpdateDisabledChildColliders(bool showError)  // returns true if there's a problem
			{
				disabledChildColliders = GetComponentsInChildren<Collider>(true);  // this gets all the colliders, even the enabled ones
				bool anyValidColliders = false; // this is probably because of old autoprobe objects or misunderstanding
				bool anyRealColliders = false;  // this is due to misunderstanding
				foreach (Collider c in disabledChildColliders)
				{
					if (c.enabled==false || c.gameObject.activeInHierarchy==false)
					{
						if (c is BoxCollider || c is SphereCollider || c is CapsuleCollider)
							anyValidColliders = true;
					}
					else
					{
						anyRealColliders = true;
					}
				}
				if (!anyValidColliders || anyRealColliders)
				{
					if (showError)
					{
						if (anyRealColliders)
							EditorUtility.DisplayDialog("Enabled Constraint Colliders Error ("+gameObject.name+")", "Do not enable the colliders under AutoProbe.  Leave them disabled, so they do not affect your scene's collision.", "I Promise To Fix It");
						else
							EditorUtility.DisplayDialog("No Constraint Colliders Error ("+gameObject.name+")", "You must keep at least one disabled child collider under AutoProbe to limit light probe generation.", "I Promise To Fix It");
					}
					return true;
				}
				return false;
			}

			// Check that the point we want to add to the set is inside a "reasonable bounds".  I broke this out into a separate function
			// in case you wanted to get fancy, or simplify the system, or whatever, without having to dig too deeply.
			// Currently, this just requires all points to fit within the DISABLED colliders which are children of this object.  That seems pretty easy and costs you nothing.
			private bool IsInsideBounds(Vector3 position)
			{
				// If the point is outside the mesh heights limitations, reject it first.
				if (meshFilters.Count>0 || terrains.Count>0)
				{
					int downHits = Physics.RaycastNonAlloc(position, Vector3.down, manyHits, maxHeightAboveMeshes, layerMask, QueryTriggerInteraction.UseGlobal);
					if (downHits==0)
						return false;  // didn't hit anything, which means we are too high or outside the vertical space of the selected meshes

					bool didNotHitCollider = true;
					foreach (RaycastHit h in manyHits)
					{
						if (meshColliders.Contains(h.collider))
							didNotHitCollider = false;
					}
					if (didNotHitCollider)  // hit other things, but not a collider we care about.  Reject it.
						return false;
				}

				if (disabledChildColliders!=null)
				{
					for (int i=0; i<disabledChildColliders.Length; i++)
					{
						Collider c = disabledChildColliders[i];
						if (c.enabled==false || c.gameObject.activeInHierarchy==false)  // do we pay attention to it?
						{
							Vector3 localPosition = c.transform.InverseTransformPoint(position);

							// Inside a sphere
							SphereCollider sc = c as SphereCollider;
							if (sc!=null)
							{
								if (Vector3.SqrMagnitude(localPosition - sc.center) <= sc.radius * sc.radius)
									return true;
							}

							// Inside a box
							BoxCollider bc = c as BoxCollider;
							if (bc!=null)
							{
								Vector3 delta = localPosition - bc.center + bc.size * 0.5f;  // offset the box by half the size, so we can do a quicker check below
								if (Vector3.Max(Vector3.zero, delta)==Vector3.Min(delta, bc.size))  // being (too?) clever
									return true;
							}

							// Inside a capsule
							CapsuleCollider cc = c as CapsuleCollider;
							if (cc!=null)
							{
								float cappedHeight = Mathf.Max(0.0f, cc.height - cc.radius*2.0f);
								float distSqToPoint;
								if (cappedHeight>0.0f)  // normal case where it is actually shaped like a capsule
								{
									Vector3 axis = (cc.direction == 0 ? Vector3.right : cc.direction == 1 ? Vector3.up : Vector3.forward) * cappedHeight;
									Vector3 p1 = cc.center - axis * 0.5f;

									// perform line test in capsule-space, where bottom capsule sphere is at 0,0,0
									Vector3 delta = localPosition - p1;
									float d = Mathf.Clamp01(Vector3.Dot(delta, axis) / (cappedHeight * cappedHeight));
									Vector3 closestPointOnLine = p1 + d * axis;
									distSqToPoint = Vector3.SqrMagnitude(closestPointOnLine - localPosition);
								}
								else  // degenerate case with height smaller than the radius of the capsule, making it a sphere
								{
									distSqToPoint = Vector3.SqrMagnitude(localPosition - cc.center);
								}

								if (distSqToPoint < cc.radius * cc.radius)
									return true;
							}
						}
					}
				}
				return false;
			}

			//-------------------
			// This finds all the known points within radius of pos and fills them out into the points list.
			private int[] sequence = { 0, -1, 1 };  // it's ideal to always check the center first, then look nearby
			private bool HasNearbyPoint(Vector3 pos, float distance)
			{
				float distanceSqr = distance*distance;
				int x = Mathf.RoundToInt(pos.x * ooResolution);
				int y = Mathf.RoundToInt(pos.y * ooResolution);
				int z = Mathf.RoundToInt(pos.z * ooResolution);
				for (int i=0; i<3; i++)  // check the cube around the bucket we find
				{
					int xm = x + sequence[i];
					for (int j = 0; j < 3; j++)
					{
						int ym = y + sequence[j];
						for (int k = 0; k < 3; k++)
						{
							int zm = z + sequence[k];
							int hash = unchecked((xm << 24) ^ (xm >> 8) ^ (ym << 16) ^ (ym >> 16) ^ (zm << 8) ^ (zm >> 24));  // probably not the world's best hash, but quick.
							List<Vector3> o = null;
							if (spatialHash.TryGetValue(hash, out o))
							{
								foreach (Vector3 v in o)
								{
									Vector3 direction = v - pos;
									float distSq = direction.sqrMagnitude;
									if (distSq<Mathf.Epsilon)  // exact point is already in set, this ray is too close to an existing one (happens with grids most of the time)
										return true;

									if (distSq < distanceSqr)
									{
										float distBetweenPoints = Mathf.Sqrt(distSq);
										// they COULD be too close.  Let's raycast and see if they are considered "visible" from one another.  If they can see each other, they're too close.
										if (Physics.RaycastNonAlloc(pos, direction/distBetweenPoints, oneHit, distBetweenPoints, layerMask, QueryTriggerInteraction.UseGlobal)==0)
											return true;  // nothing is between these two points, so they're too close
									}
								}
							}
						}
					}
				}
				return false;
			}

			// To generate a hash for a coordinate where we WANT points near each other to collide, we simply divide each coordinate by the size of the coordinate "bucket",
			// and use those values to generate a hash value directly, storing the hash as the key and adding the point to the bucket.  When we request points
			// that are nearby, we simply look at all points in the vicinity of those buckets and do the distance check there, since it should be relatively few points anyway.
			private Dictionary<int, List<Vector3>> spatialHash = new Dictionary<int, List<Vector3>>();
			private float resolution = 1.0f;  // the larger this is, the more points end up in the same bucket.  But resolution MUST be at least half the largest query size.
			private float ooResolution = 1.0f;
			private void AddToSpatialHash(Vector3 pos)
			{
				int x = Mathf.RoundToInt(pos.x * ooResolution);
				int y = Mathf.RoundToInt(pos.y * ooResolution);
				int z = Mathf.RoundToInt(pos.z * ooResolution);
				int hash = unchecked((x << 24) ^ (x >> 8) ^ (y << 16) ^ (y >> 16) ^ (z << 8) ^ (z >> 24));
				List<Vector3> o = null;
				if (!spatialHash.TryGetValue(hash, out o))
				{
					o = new List<Vector3>();
					spatialHash.Add(hash, o);
				}
				o.Add(pos);
			}

			private void InitSpatialHash(float bucketResolution)
			{
				spatialHash.Clear();
				resolution = bucketResolution;
				ooResolution = 1.0f / resolution;
			}
			//-------------------
#endif
		}
	}
}